# CodingGame

My solutions to some CodinGame contests.

- [Sopra Steria Coding Challenge (January 2021)](https://bitbucket.org/OPiMedia/codingame/src/master/Sopra-Steria-Coding-Challenge-202101/):
  bots, coding in Python
- [CodinGame Spring Challenge 2020](https://bitbucket.org/OPiMedia/codingame/src/master/CodinGame-Spring-Challenge-2020/):
  bots in a Pac-Man maze, coding in Python



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png




![gametkm](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/May/18/123001880-4-codingame-logo_avatar.png)
