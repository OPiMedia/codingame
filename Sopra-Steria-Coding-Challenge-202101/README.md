# Sopra Steria Coding Challenge (January 2021)

https://www.codingame.com/challengereport/35450056be7e857bc4b8c8eac17e61b777f9116a

Forum:
https://www.codingame.com/forum/t/sopra-steria-coding-challenge/188667

Leaderboard:
https://www.codingame.com/hackathon/sopra-steria-coding-challenge/leaderboard/global

One replay:
https://www.codingame.com/replay/525795298
