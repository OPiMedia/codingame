#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(Dirty) Solution for the
Sopra Steria Coding Challenge (January 2021)

https://www.codingame.com/challengereport/35450056be7e857bc4b8c8eac17e61b777f9116a

https://www.codingame.com/forum/t/sopra-steria-coding-challenge/188667/23

:license: GPLv3 --- Copyright (C) 2021 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: January 30, 2021
"""

import abc
import math
import random
import sys

from typing import Any, Dict, Optional, Set, Tuple


DEBUG = True

try:
    assert False
    DEBUG = False
except AssertionError:
    pass


MAP_WIDTH = 16001
MAP_HEIGHT = 9001

NB_BUSTER = 3
MAX_BUSTER_ID = NB_BUSTER * 2 - 1

MIN_NB_GHOST = 8
MAX_NB_GHOST = 13

GHOST_ROLE = -1
HUNTER_ROLE = 0
CATCHER_ROLE = 1
SUPPORT_ROLE = 2

STATE_FREE = 0
STATE_NOT_FREE = 1
STATE_STUNNED = 2
STATE_CATCH = 3
STATE_BUST = 4
STATES = (STATE_FREE, STATE_NOT_FREE, STATE_STUNNED, STATE_CATCH, STATE_BUST)

SPEED_BUSTER = 800
SPEED_GHOST = 400

DISTANCE_RELEASE = 1600
DISTANCE_STUN = 1600
DISTANCE_VISIBILITY = 2200
DISTANCE_VISIBILITY_RADAR = 4400

MIN_DISTANCE_BUST = 900
MAX_DISTANCE_BUST = 1760

MIN_DISTANCE_CATCH = 900
MAX_DISTANCE_CATCH = 1760


STUNNED_NB_TOUR = 10
STUN_RELOAD_NB_TOUR = 20

DISTANCE_GHOST_REACT = 2200

MAX_TOUR = 250


assert DISTANCE_STUN <= DISTANCE_VISIBILITY
assert MAX_DISTANCE_BUST <= DISTANCE_VISIBILITY
assert MAX_DISTANCE_CATCH <= DISTANCE_VISIBILITY


def barycentre(positions: Tuple['Point', ...]) -> 'Point':
    sum_x = 0
    sum_y = 0

    for position in positions:
        assert position.is_in_map()

        sum_x += position.x()
        sum_y += position.y()

    return Point(round(sum_x / len(positions)),
                 round(sum_y / len(positions)))


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    """
    Like print() but on stderr by default, with flush.
    Disabled if not __debug__.
    """
    if __debug__ and DEBUG:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def random_position() -> 'Point':
    return Point(random.randrange(DISTANCE_VISIBILITY,
                                  MAP_WIDTH - DISTANCE_VISIBILITY * 2),
                 random.randrange(DISTANCE_VISIBILITY,
                                  MAP_HEIGHT - DISTANCE_VISIBILITY * 2))


def square(n: int) -> int:
    return n**2  # type: ignore


class Point:
    def __init__(self, x: int, y: int) -> None:
        self._x = x
        self._y = y

    def __eq__(self, other: Any) -> bool:
        return (isinstance(other, Point) and
                (self._x == other._x) and (self._y == other._y))

    def __repr__(self) -> str:
        return repr(self.pos())

    def __str__(self) -> str:
        return str(self.pos())

    def around(self, other: 'Point', distance: int) -> 'Point':
        """
        Return the point on the segment [self, other]
        at the right distance of self.
        """
        assert distance >= 0

        x0 = self.x()
        y0 = self.y()
        x1 = other.x()
        y1 = other.y()

        if x1 != x0:
            # Slope of the line
            m = (y1 - y0) / (x1 - x0)

            # Two points on the line and on the circle
            q = distance / math.sqrt(m**2 + 1)
            xp = x0 + q
            xm = x0 - q

            yp = m * (xp - x0) + y0
            ym = m * (xm - x0) + y0

            pp = Point(round(xp), round(yp))
            pm = Point(round(xm), round(ym))

            return (pp if (Point(x1, y1).distance2(pp) <=
                           Point(x1, y1).distance2(pm))
                    else pm)

        if y0 <= y1:
            return Point(x0, y0 + distance)

        return Point(x0, y0 - distance)

    def distance(self, other: 'Point') -> float:
        return math.sqrt(self.distance2(other))

    def distance2(self, other: 'Point') -> int:
        return (square(self.x() - other.x()) +
                square(self.y() - other.y()))

    def is_in_annulus(self, center: 'Point',
                      smaller: int, bigger: int) -> bool:
        assert 0 <= smaller <= bigger

        return smaller**2 < self.distance2(center) <= bigger**2

    def is_in_disc(self, center: 'Point', distance: int) -> bool:
        assert distance >= 0

        return self.distance2(center) <= distance**2

    def is_in_map(self) -> bool:
        return (0 <= self.x() < MAP_WIDTH) and (0 <= self.y() < MAP_HEIGHT)

    def pos(self) -> Tuple[int, int]:
        return (self._x, self._y)

    def symmetric(self) -> 'Point':
        return Point(MAP_WIDTH - 1 - self._x, MAP_HEIGHT - 1 - self._y)

    def x(self) -> int:
        return self._x

    def y(self) -> int:
        return self._y


BASE_0_POSITION = Point(0, 0)                           # top left
BASE_1_POSITION = Point(MAP_WIDTH - 1, MAP_HEIGHT - 1)  # bottom right
BASES_POSITIONS = (BASE_0_POSITION, BASE_1_POSITION)


class Path:  # pylint: disable=too-few-public-methods
    def __init__(self, distance: int, points: Tuple[Point, ...]) -> None:
        self._distance = distance
        self._points = points

        self._i = 0

    def distance(self) -> int:
        return self._distance

    def move(self, point: Point) -> Point:
        if point.is_in_disc(self._points[self._i], self._distance):
            self._i = (self._i + 1) % len(self._points)

        return self._points[self._i]

    def points(self) -> Tuple[Point, ...]:
        return self._points


class Entity(abc.ABC):  # pylint: disable=too-many-instance-attributes
    def __init__(self, game: 'Game', entity_id: int) -> None:
        assert 0 <= entity_id <= max(MAX_BUSTER_ID, MAX_NB_GHOST - 1)

        self._game = game

        self._entity_id = entity_id
        self._position = Point(-1, -1)
        self._team_id = -1
        self._role = -1
        self._state = 1000
        self._value = -1

        self._is_visible = False
        self._not_visible_since = 0

    def __str__(self) -> str:
        return f'{self.__class__.__name__} {self.entity_id()} {self.position()} {self._state} {self._value} {"*" if self.is_visible() else ""}{self._not_visible_since}'  # noqa

    def around(self, point: Point, distance: int) -> Point:
        assert distance >= 0

        return self.position().around(point, distance)

    def default_position(self) -> Point:
        ghosts = self.ghosts_visible_sorted()
        if ghosts:
            return ghosts[0].position()

        opponents = self.opponents_visible_sorted()
        if opponents:
            return opponents[0].position()

        return random_position()

    def distance2(self, point: Point) -> int:
        assert point.is_in_map()

        return self.position().distance2(point)

    def entity_id(self) -> int:
        return self._entity_id  # type: ignore  # false positive with pytype

    def game(self) -> 'Game':
        return self._game

    def ghosts_can_be_bust(self) -> Tuple['Ghost', ...]:
        return tuple(ghost
                     for ghost in sorted(
                             self.game().ghosts_visible(),
                             key=lambda ghost: (
                                 ghost.stamina() > 5,
                                 self.position().distance2(
                                     ghost.possible_next_position()),
                                 ghost.stamina(),
                                 ghost.entity_id()))
                     if ghost.can_be_bust(self.position()))

    def ghosts_can_be_catch(self) -> Tuple['Ghost', ...]:
        return tuple(ghost
                     for ghost in sorted(
                             self.game().ghosts_visible(),
                             key=lambda ghost: (
                                 self.position().distance2(
                                     ghost.possible_next_position()),
                                 ghost.entity_id()))
                     if ghost.can_be_catch(self.position()))

    def ghosts_possible_sorted(self) -> Tuple['Ghost', ...]:
        return tuple(sorted(
            [ghost for ghost in self.game().ghosts()
             if ghost.position().is_in_map()],
            key=lambda ghost: (
                ghost.stamina(),
                ghost._not_visible_since // 2,
                self.position().distance2(ghost.possible_next_position()),
                ghost._not_visible_since,
                ghost.entity_id())))

    def ghosts_visible_sorted(self) -> Tuple['Ghost', ...]:
        return tuple(sorted(
            self.game().ghosts_visible(),
            key=lambda ghost: (
                ghost.stamina(),
                self.position().distance2(ghost.possible_next_position()),
                ghost.entity_id())))

    def is_visible(self) -> bool:
        return self._is_visible

    def opponents_can_be_stun(self) -> Tuple['Buster', ...]:
        return tuple(opponent for opponent in self.opponents_visible_sorted()
                     if ((opponent.entity_id() % 3 != 0) and
                         opponent.can_be_stun(self.position())))

    def opponents_possible_sorted(self) -> Tuple['Buster', ...]:
        return tuple(sorted(
            [opponent for opponent in self.game().opponents_visible()
             if ((opponent.entity_id() != 0) and
                 opponent.position().is_in_map())],
            key=lambda opponent: (
                self.position().distance2(opponent.position()),
                opponent._not_visible_since,
                opponent.entity_id())))

    def opponents_visible_sorted(self) -> Tuple['Buster', ...]:
        return tuple(sorted(self.game().opponents_visible(),
                            key=lambda opponent: (
                                self.position().distance2(opponent.position()),
                                -opponent.entity_id())))

    def reset(self) -> None:
        self.set_visible(False)

    def set_position(self, position: Point) -> None:
        self._position = position

    def set_visible(self, visible: bool) -> None:
        self._is_visible = visible
        if visible:
            self._not_visible_since = 0
        else:
            self._not_visible_since += 1

    def position(self) -> Point:
        return self._position

    @abc.abstractmethod
    def update_from_data(self,  # pylint: disable=too-many-arguments
                         entity_id: int, x: int, y: int, team_id: int,
                         role: int, state: int, value: int) -> None:
        assert self._entity_id == entity_id
        assert 0 <= x < MAP_WIDTH
        assert 0 <= y < MAP_HEIGHT
        assert -1 <= team_id <= 1
        assert -1 <= role <= 2
        assert state >= 0
        assert -1 <= value <= max(STUN_RELOAD_NB_TOUR, MAX_NB_GHOST - 1)  # ??? -1?  # noqa

        self._position = Point(x, y)
        self._team_id = team_id
        self._role = role
        self._state = state
        self._value = value

        self.set_visible(True)

    def x(self) -> int:
        return self.position().x()

    def y(self) -> int:
        return self.position().y()


class Buster(Entity):
    def __init__(self, game: 'Game', entity_id: int) -> None:
        super().__init__(game, entity_id)

        self._target_ghost: Optional['Ghost'] = None
        self._target_position = self.position()
        self._default_path = Path(0, tuple())

        self._stunned = 0

        self._command = ''
        self._message = ''

    def __str__(self) -> str:
        return f'{self.__class__.__name__} {self.entity_id()} {self.position()} {self._state} {self._value} {"*" if self.is_visible() else ""}{self._not_visible_since} {self._stunned}'  # noqa

    def base(self) -> Point:
        return BASES_POSITIONS[self.team_id()]

    def can_be_stun(self, point: Point) -> bool:
        assert point.is_in_map()

        return self.position().is_in_disc(point, DISTANCE_STUN)

    def command_move(self, point: Point) -> str:  # pylint: disable=no-self-use
        dist = max(MIN_DISTANCE_BUST, MIN_DISTANCE_CATCH) * 3 // 4

        x, y = point.pos()

        if x < 0:
            x = dist
        elif x >= MAP_WIDTH:
            x = MAP_WIDTH - dist

        if y < 0:
            y = dist
        elif y >= MAP_HEIGHT:
            y = MAP_HEIGHT - dist

        assert Point(x, y).is_in_map()

        self._target_position = Point(x, y)

        return f'MOVE {x} {y}'

    def default_path_move(self) -> Point:
        return self._default_path.move(self.position())

    def is_in_base(self) -> bool:
        return self.position().is_in_disc(self.base(), DISTANCE_RELEASE)

    def is_stun(self) -> bool:
        return self._state == 2

    def opponent_base(self) -> Point:
        return BASES_POSITIONS[min(1, 1 - self.team_id())]

    def opposite_id(self) -> int:
        return (NB_BUSTER + self.entity_id() if self.entity_id() < NB_BUSTER
                else self.entity_id() % NB_BUSTER)

    def play_command_to_str(self, command: str, message: str) -> str:
        log(str(self), command, message)

        return (f'{command} {command} {message}' if DEBUG
                else command)

    @abc.abstractmethod
    def play_to_str(self) -> str:
        if self.is_stun():
            self._command = self.command_move(self.position())
            self._message = 'STUNNED'
        else:
            self._command = self.command_move(self.default_path_move())
            self._message = '#'

        return self.play_command_to_str(self._command, self._message)

    def reset(self) -> None:
        super().reset()

        self.set_target_ghost(None)
        if self._stunned >= 1:
            self._stunned -= 1

    def set_target_ghost(self, ghost: Optional['Ghost']) -> None:
        self._target_ghost = ghost

    def target_ghost(self) -> Optional['Ghost']:
        return self._target_ghost

    def target_position(self) -> Point:
        return self._target_position

    def team_id(self) -> int:
        return self._team_id

    @abc.abstractmethod
    def update_from_data(self,  # pylint: disable=too-many-arguments
                         entity_id: int, x: int, y: int, team_id: int,
                         role: int, state: int, value: int) -> None:
        super().update_from_data(entity_id, x, y, team_id,
                                 role, state, value)

        assert 0 <= entity_id <= MAX_BUSTER_ID
        assert team_id in (0, 1)
        assert role in (HUNTER_ROLE, CATCHER_ROLE, SUPPORT_ROLE)
        assert state in STATES


class BusterHunter(Buster):
    """
    MOVE x y
    BUST id: ghost.state -=1 if MIN_DISTANCE_BUST <= distance <= MAX_DISTANCE_BUST
    """  # noqa

    def command_bust(self, ghost: 'Ghost') -> str:
        assert ghost.can_be_bust(self.position())

        return f'BUST {ghost.entity_id()}'

    def play_to_str(self) -> str:
        super().play_to_str()

        if self.is_stun():
            return self.play_command_to_str(self._command, self._message)

        ghosts = self.ghosts_can_be_bust()
        if ghosts and (self._value < 2):  # Bust a ghost in the right distance
            ghost = ghosts[0]
            self._command = self.command_bust(ghost)
            self._message = f'Bg{ghost.entity_id()}'
            self.set_target_ghost(ghost)
        else:
            ghosts = tuple(ghost for ghost in self.ghosts_possible_sorted()
                           if ghost.stamina() != 0)
            if ghosts:                    # Move to a ghost
                ghost = ghosts[0]
                self._command = self.command_move(
                    self.around(ghost.possible_next_position(),
                                (MIN_DISTANCE_BUST + MAX_DISTANCE_BUST) // 2))
                self._message = f'>g{ghost.entity_id()} {self.position().distance(ghost.position()):.1f}'  # noqa
                self.set_target_ghost(ghost)

        return self.play_command_to_str(self._command, self._message)

    def update_from_data(self,  # pylint: disable=too-many-arguments
                         entity_id: int, x: int, y: int, team_id: int,
                         role: int, state: int, value: int) -> None:
        super().update_from_data(entity_id, x, y, team_id,
                                 role, state, value)

        assert entity_id % NB_BUSTER == 0
        assert state in (STATE_FREE, STATE_NOT_FREE, STATE_STUNNED, STATE_BUST)
        if state == STATE_STUNNED:
            assert 0 <= value <= STUNNED_NB_TOUR  # ??? -1
        else:
            assert value == -1

        if self._default_path.distance() == 0:
            x0 = DISTANCE_VISIBILITY
            x1 = MAP_WIDTH - DISTANCE_VISIBILITY
            y0 = DISTANCE_VISIBILITY
            y1 = MAP_HEIGHT - DISTANCE_VISIBILITY
            points = (Point(x0, y0), Point(x1, y0),
                      Point(x1, y1), Point(x0, y1))
            if self.team_id() == 1:
                points = points[-2:] + points[:2]
            self._default_path = Path(DISTANCE_VISIBILITY, points)


class BusterCatcher(Buster):
    """
    MOVE x y
    TRAP id: catch if ghost.state == 0 and MIN_DISTANCE_CATCH <= distance <= MAX_DISTANCE_CATCH
    RELEASE: release the caught ghost. Win 1 point if distance to base <= DISTANCE_RELEASE
    """  # noqa

    def command_catch(self, ghost: 'Ghost') -> str:
        assert ghost.can_be_catch(self.position())

        return f'TRAP {ghost.entity_id()}'

    def command_release(self) -> str:  # pylint: disable=no-self-use
        assert not self.is_free()
        assert self.is_in_base()

        ghost = self.game().ghosts()[self.ghost_id()]
        out = Point(-1, -1)
        ghost.set_position(out)
        ghost.set_possible_next_position(out)

        return 'RELEASE'

    def ghost_id(self) -> int:
        assert self.is_free() or (0 <= self._value < MAX_NB_GHOST)

        return self._value

    def is_free(self) -> bool:
        return self._state == 0

    def play_to_str(self) -> str:
        super().play_to_str()

        if self.is_stun():
            return self.play_command_to_str(self._command, self._message)

        if self.is_free():
            ghosts = self.ghosts_can_be_catch()
            if ghosts:       # Catch a ghost
                self._command = self.command_catch(ghosts[0])
                self._message = f'Cg{ghosts[0].entity_id()}'
            else:
                ghosts = tuple(ghost
                               for ghost in self.ghosts_possible_sorted()
                               if ghost.stamina() <= 5)
                target_ghost = self.game().busters()[0].target_ghost()
                stamina = (ghosts[0].stamina() if ghosts
                           else 5)
                which = 's'
                if ((target_ghost is not None) and
                        (target_ghost.stamina() < stamina)):
                    ghosts = (target_ghost, )
                    which = '_'
                if ghosts:   # Move to a ghost
                    ghost = ghosts[0]
                    self._command = self.command_move(
                        ghost.possible_next_position().around(
                            self.position(),
                            (MIN_DISTANCE_CATCH + MAX_DISTANCE_CATCH) // 2))
                    self._message = f'>g{which}{ghost.entity_id()} {self.position().distance(ghost.position()):.1f}'  # noqa
        elif self.is_in_base():  # Release a captured ghost to the base
            self._command = self.command_release()
            self._message = ''
        else:                    # Move to the base
            self._command = self.command_move(
                self.around(BASES_POSITIONS[self.team_id()],
                            DISTANCE_RELEASE - 5))
            self._message = '>b'

        return self.play_command_to_str(self._command, self._message)

    def update_from_data(self,  # pylint: disable=too-many-arguments
                         entity_id: int, x: int, y: int, team_id: int,
                         role: int, state: int, value: int) -> None:
        super().update_from_data(entity_id, x, y, team_id,
                                 role, state, value)

        assert entity_id % NB_BUSTER == 1
        assert state in (STATE_FREE, STATE_NOT_FREE, STATE_STUNNED, STATE_CATCH)  # noqa
        if state == STATE_STUNNED:
            assert 0 <= value <= STUNNED_NB_TOUR  # ??? -1
        else:
            assert -1 <= value <= MAX_NB_GHOST

        if self._default_path.distance() == 0:
            self._default_path = Path(DISTANCE_VISIBILITY,
                                      (Point(MAP_WIDTH - 1000, 1000),
                                       self.opponent_base(),
                                       Point(1000, MAP_HEIGHT - 1000),
                                       self.base()))


class BusterSupport(Buster):
    """
    MOVE x y
    STUN id: stun a buster (and release its ghost) during STUNNED_NB_TOUR. Disabled during STUN_RELOAD_NB_TOUR.
    RADAR: increase its visibility to DISTANCE_VISIBILITY_RADAR. Only once by game.
    """  # noqa

    def __init__(self, game: 'Game', entity_id: int) -> None:
        super().__init__(game, entity_id)

        self._has_radar = True
        self._reload = 0

    def __str__(self) -> str:
        return f'{self.__class__.__name__} {self.entity_id()} {self.position()} {self._state} {self._value} {"*" if self.is_visible() else ""}{self._not_visible_since} {self._stunned} {self._reload} {self._has_radar}'  # noqa

    def can_stun(self) -> bool:
        return self.stun_reload() == 0

    def command_radar(self) -> str:  # pylint: disable=no-self-use
        return 'RADAR'

    def command_stun(self, buster: Buster) -> str:
        assert buster.can_be_stun(self.position())  # noqa

        self._reload = STUN_RELOAD_NB_TOUR
        buster._stunned = STUNNED_NB_TOUR

        return f'STUN {buster.entity_id()}'

    def has_radar(self) -> bool:
        return self._has_radar

    def stun_reload(self) -> int:
        return self._reload

    def play_to_str(self) -> str:
        super().play_to_str()

        if self.is_stun():
            return self.play_command_to_str(self._command, self._message)

        to_stun = False

        catcher = self.game().busters()[1]

        assert isinstance(catcher, BusterCatcher)

        if (catcher.is_free() and (catcher._stunned < 5) and
                (self.game().tour() > 9)):
            # Move to front of catcher
            self._command = self.command_move(
                catcher.target_position().around(self.position(),
                                                 DISTANCE_VISIBILITY * 2))
            self._message = f'>c {self.position().distance(catcher.position()):.1f}'  # noqa

        if (self.has_radar() and (
                not self.position().is_in_disc(
                    self.game().busters()[0].position(),
                    DISTANCE_VISIBILITY) and
                not self.position().is_in_disc(
                    self.game().busters()[1].position(),
                    DISTANCE_VISIBILITY) and
                not self.game().ghosts_visible() and
                not self.game().opponents_visible())):
            #                                    Use radar
            self._has_radar = False
            self._command = self.command_radar()
            self._message = ''
        else:
            opponents = self.opponents_can_be_stun()
            if self.can_stun() and opponents:  # Stun an opponent
                opponent = opponents[0]
                self._command = self.command_stun(opponent)
                self._message = f'So{opponent.entity_id()}'
                to_stun = True
            elif self.stun_reload() < 5:
                opponents = tuple(
                    opponent for opponent in self.opponents_possible_sorted()
                    if ((opponent.entity_id() % 3 != 0) and
                        (opponent._stunned < 3)))
                if opponents:                  # Move to an opponent
                    opponent = opponents[0]
                    self._command = self.command_move(
                        self.around(
                            opponent.position(), DISTANCE_STUN // 2))
                    self._message = f'>o{opponent.entity_id()}'

        opponent_catcher = self.game().opponents()[1]
        opponent_support = self.game().opponents()[2]

        assert isinstance(opponent_catcher, BusterCatcher)
        assert isinstance(opponent_support, BusterSupport)

        if (not to_stun and (self.stun_reload() < 5) and
            opponent_support.position().is_in_map() and
            (catcher.position().distance2(opponent_support.position()) <=
             catcher.position().distance2(self.position()))):
            # Move between catcher and support opponent
            x = (catcher.position().x() + opponent_support.position().x()) // 2
            y = (catcher.position().y() + opponent_support.position().y()) // 2
            self._command = self.command_move(Point(x, y))
            self._message = '>between'

        if (not to_stun and (self.stun_reload() < 5) and
                opponent_catcher.position().is_in_map() and
                not opponent_catcher.is_free()):
            # Move to catcher opponent
            self._command = self.command_move(opponent_catcher.position())
            self._message = f'>C {self.position().distance(opponent_catcher.position()):.1f}'  # noqa

        return self.play_command_to_str(self._command, self._message)

    def reset(self) -> None:
        super().reset()

        if self._reload >= 1:
            self._reload -= 1

    def update_from_data(self,  # pylint: disable=too-many-arguments
                         entity_id: int, x: int, y: int, team_id: int,
                         role: int, state: int, value: int) -> None:
        super().update_from_data(entity_id, x, y, team_id,
                                 role, state, value)

        assert entity_id % NB_BUSTER == 2
        assert state in (STATE_FREE, STATE_NOT_FREE, STATE_STUNNED)
        if state == STATE_STUNNED:
            assert 0 <= value <= STUNNED_NB_TOUR  # ??? -1
        else:
            assert -1 <= value <= STUN_RELOAD_NB_TOUR  # ??? -1?

        if self._default_path.distance() == 0:
            self._default_path = Path(DISTANCE_VISIBILITY,
                                      (self.opponent_base(),
                                       Point(MAP_WIDTH - 1000, 1000),
                                       self.base(),
                                       Point(1000, MAP_HEIGHT - 1000)))


class Ghost(Entity):
    """
    If bust, then don't move.

    Else if all busters far enough (distance > DISTANCE_GHOST_REACT),
    then move to start position (or don't move).

    Else run away to opposite of barycenter of close busters.
    """

    def __init__(self, game: 'Game', entity_id: int) -> None:
        super().__init__(game, entity_id)

        assert 0 <= entity_id < MAX_NB_GHOST

        self._possible_next_position = self._position

    def __str__(self) -> str:
        return f'{self.__class__.__name__} {self.entity_id()} {self.position()} {self._possible_next_position} {self._state} {self._value} {"*" if self.is_visible() else ""}{self._not_visible_since}'  # noqa

    def can_be_bust(self, point: Point) -> bool:
        assert point.is_in_map()

        return ((self.stamina() != 0) and
                self.position().is_in_annulus(point, MIN_DISTANCE_BUST,
                                              MAX_DISTANCE_BUST))

    def can_be_catch(self, point: Point) -> bool:
        assert point.is_in_map()

        return ((self.stamina() == 0) and
                self.position().is_in_annulus(point, MIN_DISTANCE_CATCH,
                                              MAX_DISTANCE_CATCH))

    def nb_try_to_catch(self) -> int:
        return self._value

    def possible_next_position(self) -> Point:
        return self._possible_next_position

    def reset(self) -> None:
        super().reset()

        self._position = self._possible_next_position

    def set_possible_next_position(self, position: Point) -> None:
        x = min(MAP_WIDTH - 1, max(0, position.x()))
        y = min(MAP_HEIGHT - 1, max(0, position.y()))

        self._possible_next_position = Point(x, y)

    def stamina(self) -> int:
        return self._state

    def update_from_data(self,  # pylint: disable=too-many-arguments
                         entity_id: int, x: int, y: int, team_id: int,
                         role: int, state: int, value: int) -> None:
        super().update_from_data(entity_id, x, y, team_id,
                                 role, state, value)

        assert 0 <= entity_id < MAX_NB_GHOST
        assert team_id == -1
        assert role == GHOST_ROLE
        assert 0 <= value <= 2 * 2


class Game:
    def __init__(self, nb_ghost: int, my_team_id: int) -> None:
        assert MIN_NB_GHOST <= nb_ghost <= MAX_NB_GHOST
        assert my_team_id in (0, 1)

        self._nb_ghost = nb_ghost
        self._my_team_id = my_team_id

        self._tour = 0

        if my_team_id == 0:
            my_id = 0
            opponend_id = NB_BUSTER
        else:
            my_id = NB_BUSTER
            opponend_id = 0

        self._busters: Tuple[Buster, Buster, Buster] = (
            BusterHunter(self, my_id),
            BusterCatcher(self, my_id + 1),
            BusterSupport(self, my_id + 2))
        self._opponents: Tuple[Buster, Buster, Buster] = (
            BusterHunter(self, opponend_id),
            BusterCatcher(self, opponend_id + 1),
            BusterSupport(self, opponend_id + 2))
        self._ghosts: Tuple[Ghost, ...] = tuple(
            Ghost(self, i) for i in range(self.nb_ghost()))

        self._entities: Dict[int, Tuple[Entity, ...]] = {
            -1: self._ghosts,
            0: (self._busters if my_team_id == 0
                else self._opponents),
            1: (self._busters if my_team_id != 0
                else self._opponents)}

    def busters(self) -> Tuple[Buster, Buster, Buster]:
        return self._busters

    def entities(self) -> Dict[int, Tuple[Entity, ...]]:
        return self._entities

    def ghosts(self) -> Tuple[Ghost, ...]:
        return self._ghosts

    def ghosts_visible(self) -> Tuple[Ghost, ...]:
        return tuple(ghost for ghost in self._ghosts
                     if ghost.is_visible())

    def nb_ghost(self) -> int:
        return self._nb_ghost

    def opponent_base(self) -> Point:
        return BASES_POSITIONS[min(1, 1 - self.busters()[0].team_id())]

    def opponents(self) -> Tuple[Buster, Buster, Buster]:
        return self._opponents

    def opponents_visible(self) -> Tuple[Buster, ...]:
        return tuple(opponent for opponent in self._opponents
                     if opponent.is_visible())

    def run(self) -> None:
        running = True
        while running:
            running = self.run_tour()

    def run_tour(self) -> bool:  # pylint: disable=too-many-locals
        self._tour += 1

        assert self._tour <= MAX_TOUR + 1

        try:
            nb_entity_visible = int(input())
        except EOFError:
            return False

        for buster in self.busters():
            buster.reset()

        for opponent in self.opponents():
            opponent.reset()

        for ghost in self.ghosts():
            ghost.reset()

        # To check ids of each busters team are consecutive 0, 1, 2 or 3, 4, 5
        already: Dict[int, Set[int]] = {-1: set(),
                                        0: set(),
                                        1: set()}

        # Read data from input
        for _ in range(nb_entity_visible):
            line = input()
            # log(line)
            entity_id, x, y, team_id, role, state, value = map(int,
                                                               line.split())

            assert entity_id not in already[team_id]

            already[team_id].add(entity_id)

            self.entities()[team_id][entity_id % NB_BUSTER
                                     if team_id != -1
                                     else entity_id].update_from_data(
                entity_id, x, y, team_id, role, state, value)

        # Update data computed
        if self.tour() == 1:
            self.set_initial_positions()
        self.update_possible_next_positions()

        # Some checking
        assert len(already[-1]) <= MAX_NB_GHOST

        if __debug__:
            for i, buster in enumerate(self.busters()):
                assert buster.entity_id() % NB_BUSTER == i
            for i, opponent in enumerate(self.opponents()):
                assert opponent.entity_id() % NB_BUSTER == i
            for i, ghost in enumerate(self.ghosts()):
                assert ghost.entity_id() == i

            for team_id in (0, 1):
                assert len(already[team_id]) <= NB_BUSTER
                assert (all(0 <= buster_id < NB_BUSTER for buster_id in already[team_id]) or all(NB_BUSTER <= buster_id < NB_BUSTER * 2 for buster_id in already[team_id]))  # noqa

        # Play each buster
        for buster in self.busters():
            print(buster.play_to_str())

        # Log opponents and ghosts
        for opponent in self.opponents():
            log(str(opponent))

        for buster in self.busters():
            log([str(ghost) for ghost in buster.ghosts_possible_sorted()])

        # Update positions from possible next positions
        self.update_positions()

        return True

    def set_initial_positions(self) -> None:
        for opponent in self.opponents():
            if not opponent.position().is_in_map():
                opponent.set_position(
                    self.busters()[opponent.opposite_id() % 3]
                    .position().symmetric())

        for ghost in self.ghosts():
            if self.nb_ghost() % 2 == 0:
                other_id = (ghost.entity_id() + 1 if ghost.entity_id() % 2 == 0
                            else ghost.entity_id() - 1)
            else:
                other_id = (ghost.entity_id() - 1 if ghost.entity_id() % 2 == 0
                            else ghost.entity_id() + 1)
            other_ghost = self.ghosts()[other_id]
            if ((other_id >= 0) and not ghost.position().is_in_map() and
                    other_ghost.position().is_in_map()):
                ghost.set_position(other_ghost.position().symmetric())

    def tour(self) -> int:
        return self._tour

    def update_positions(self) -> None:
        for ghost in self.ghosts():
            ghost.set_position(ghost.possible_next_position())

    def update_possible_next_positions(self) -> None:
        for opponent in self.opponents():
            if (opponent.position().is_in_map() and
                    (opponent._not_visible_since > 3)):
                must_be_visible = False
                for buster in self.busters():
                    if buster.position().is_in_disc(opponent.position(),
                                                    DISTANCE_VISIBILITY):
                        must_be_visible = True

                        break

                if must_be_visible:
                    opponent.set_position(Point(-1, -1))

        for ghost in self.ghosts():
            if (ghost.position().is_in_map() and
                self.opponent_base().is_in_disc(ghost.position(),
                                                DISTANCE_RELEASE)):
                ghost.set_position(Point(-1, -1))

            if ghost.position().is_in_map() and (ghost._not_visible_since > 3):
                must_be_visible = False
                for buster in self.busters():
                    if buster.position().is_in_disc(ghost.position(),
                                                    DISTANCE_VISIBILITY):
                        must_be_visible = True

                        break

                if must_be_visible:
                    ghost.set_position(Point(-1, -1))

            in_same_place = False
            for opponent in self.opponents():
                if ghost.position() == opponent.position():
                    in_same_place = True

                    break

            if ghost.position().is_in_map() and not in_same_place:
                points = (tuple(buster.position()
                                for buster in self.busters()) +
                          tuple(opponent.position()
                                for opponent in self.opponents()
                                if opponent.position().is_in_map()))
                points = tuple(
                    point for point in points
                    if ghost.position().is_in_disc(point,
                                                   DISTANCE_GHOST_REACT))
                if points:
                    point = barycentre(points)
                    destination = point.around(
                        ghost.position(),
                        int(ghost.position().distance(point) * 2))
                    ghost.set_possible_next_position(destination)
            else:
                ghost.set_possible_next_position(ghost.position())


def main() -> None:
    random.seed(666)

    nb_buster = int(input())

    assert nb_buster == NB_BUSTER

    game = Game(int(input()), int(input()))
    game.run()


if __name__ == '__main__':
    main()
