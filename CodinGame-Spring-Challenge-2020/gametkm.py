#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
gametkm.

GUI in Tk
for the engine of CodingGame Spring Challenge 2020.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: May 18, 2020
"""

import random
import sys
import tkinter

from typing import Callable, Optional, Tuple

import botm
import gamem


#
# Classes
#########
class GameTk:  # noqa  # pylint: disable=too-few-public-methods,too-many-instance-attributes
    BOT_COLORS = ('#FF6060', '#6060FF')

    def __init__(self, maze: botm.Maze,  # noqa  # pylint: disable=too-many-locals,too-many-statements
                 nb_pac_by_bot: int, nb_super_pellet_by_symmetry: int,
                 nb_tour: int = 200):
        assert nb_pac_by_bot > 0
        assert nb_super_pellet_by_symmetry >= 0
        assert nb_tour >= 0

        self._game = gamem.Game(maze, nb_pac_by_bot,
                                nb_super_pellet_by_symmetry, nb_tour=nb_tour)

        self._cell_size = 37

        self._master = tkinter.Tk()
        self._master.title('Solution to CodingGame Spring Challenge 2020')
        self._master.resizable(False, False)
        self._master.geometry('1600x1000')
        self._master.protocol('WM_DELETE_WINDOW', lambda: sys.exit(0))

        # Create widgets
        # # Score
        frame = tkinter.Frame(self._master)
        frame.pack()

        self._text_scores_tk = []
        for i in range(2):
            if i > 0:
                text = tkinter.Label(frame)
                text.config(text=' - ')
                text.pack(side=tkinter.LEFT)

            score_tk = tkinter.StringVar()
            score_tk.set('0')
            text = tkinter.Label(frame,
                                 foreground=GameTk.BOT_COLORS[i])
            text.config(textvariable=score_tk)
            text.pack(side=tkinter.LEFT)
            self._text_scores_tk.append(score_tk)

        # # Canvas
        self._canvas = tkinter.Canvas(
            self._master,
            width=self._cell_size * self._game.maze.width,
            height=self._cell_size * self._game.maze.height)  # type: tkinter.Canvas  # noqa
        self._canvas.pack()

        # # Slide bar
        self._tour_tk = tkinter.IntVar(0)
        self._slide = tkinter.Scale(self._master, orient='horizontal',
                                    from_=0, to=self._game.nb_tour,
                                    resolution=1, tickinterval=5,
                                    variable=self._tour_tk)
        self._slide.pack(fill='x')
        self._slide.bind('<ButtonRelease-1>',
                         lambda _: self._button_play(
                             lambda: self._game.play(self._tour_tk.get())))

        # # Tour
        frame = tkinter.Frame(self._master)
        frame.pack()

        self._tour_label = tkinter.Label(frame)
        self._tour_label.pack(side=tkinter.LEFT)

        # # Play functions
        def go_beginning():
            self._button_play(lambda: self._game.play(0))

        def go_backward_1():
            self._button_play(
                lambda: self._game.play(max(0, self._game.tour - 1)))

        def go_backward_10():
            self._button_play(
                lambda: self._game.play(max(0, self._game.tour - 10)))

        def go_end():
            self._button_play(lambda: self._game.play(self._game.nb_tour))

        def go_forward_1():
            self._button_play(
                lambda: self._game.play(min(self._game.nb_tour,
                                            self._game.tour + 1)))

        def go_forward_10():
            self._button_play(
                lambda: self._game.play(min(self._game.nb_tour,
                                            self._game.tour + 10)))

        # # Buttons
        for text, function in (('\u23EE', go_beginning),
                               ('\u23EA', go_backward_10),
                               ('\u23F4', go_backward_1),
                               ('\u25B6', go_forward_1),
                               ('\u23E9', go_forward_10),
                               ('\u23ED', go_end)):
            tkinter.Button(frame, text=text, command=function).pack(
                side=tkinter.LEFT)

        subframes = (tkinter.Frame(frame), tkinter.Frame(frame))
        subframes[0].pack(side=tkinter.LEFT)
        subframes[1].pack(side=tkinter.LEFT)

        self._draw_path_checks_tk = (
            tkinter.BooleanVar(value=True),
            tkinter.BooleanVar(value=False))  # type: Tuple[tkinter.BooleanVar, tkinter.BooleanVar]  # noqa
        for bot_id in range(2):
            tkinter.Checkbutton(subframes[bot_id],
                                text='path {}'.format(bot_id),
                                variable=self._draw_path_checks_tk[bot_id],
                                command=self._button_play).pack()

        self._draw_paths_checks_tk = (
            tkinter.BooleanVar(value=True),
            tkinter.BooleanVar(value=False))  # type: Tuple[tkinter.BooleanVar, tkinter.BooleanVar]  # noqa
        for bot_id in range(2):
            tkinter.Checkbutton(subframes[bot_id],
                                text='paths {}'.format(bot_id),
                                variable=self._draw_paths_checks_tk[bot_id],
                                command=self._button_play).pack()

        # # Keys
        self._master.bind('<KeyRelease-KP_Home>', lambda _: go_beginning())
        self._master.bind('<KeyRelease-KP_Prior>', lambda _: go_backward_10())
        self._master.bind('<KeyRelease-Left>', lambda _: go_backward_1())

        self._master.bind('<KeyRelease-space>', lambda _: go_forward_1())
        self._master.bind('<KeyRelease-Right>', lambda _: go_forward_1())
        self._master.bind('<KeyRelease-KP_Next>', lambda _: go_forward_10())
        self._master.bind('<KeyRelease-KP_End>', lambda _: go_end())

        self._master.bind('<KeyRelease-Escape>', lambda _: sys.exit())

        # # Textual info on pacs
        frame = tkinter.Frame(self._master)
        frame.pack(fill='x')
        self._info_labels = []

        for bot_id in range(2):
            subframe = tkinter.Frame(frame)
            subframe.pack(side=(tkinter.LEFT if bot_id == 0
                                else tkinter.RIGHT))

            info_label = tkinter.Label(subframe, text='', justify=tkinter.LEFT)
            info_label.pack()
            self._info_labels.append(info_label)

        # Draw
        self._draw()
        self._game.display_function = lambda: self._draw()  # noqa  # pylint: disable=unnecessary-lambda

        # Events on canvas
        text_pos_id = self._canvas.create_text(50, 50, fill='white', text='')

        def canvas_motion(event):
            pos = botm.Pos(event.x // self._cell_size,
                           event.y // self._cell_size)
            self._canvas.itemconfig(text_pos_id, text='({})'.format(pos))

            current_pos_x, current_pos_y = self._canvas.coords(text_pos_id)
            self._canvas.move(text_pos_id, event.x - current_pos_x,
                              event.y - current_pos_y - 5)

            self._canvas.tag_raise(text_pos_id)

        self._canvas.bind('<Leave>',
                          lambda _: self._canvas.itemconfig(text_pos_id,
                                                            text=''))
        self._canvas.bind('<Motion>', canvas_motion)

    def _button_play(self, play_function: Optional[Callable[[], None]] = None):
        self._master.quit()

        if play_function is not None:
            play_function()

        self._master.mainloop()

    def _draw(self):
        self._draw_maze()

        self._tour_tk.set(self._game.tour)
        self._tour_label.config(text='({:.2f}ms - {:.2f}ms)   {}/{}   '
                                .format(self._game.bots[0].duration,
                                        self._game.bots[1].duration,
                                        self._game.tour,
                                        self._game.nb_tour))
        self._slide.config(to=self._game.nb_tour)

        for bot in self._game.bots:
            bot_id = bot.bot_id

            # Draw game
            for pac in self._game.pacs[bot_id]:
                if pac.is_alive:
                    self._draw_pacgame(pac)

            self._text_scores_tk[bot_id].set(self._game.scores[bot_id])

            # Print information
            if bot_id == 0:
                pacs_a = bot.pac_mines
                pacs_b = bot.pac_opponents
            else:
                pacs_a = bot.pac_opponents
                pacs_b = bot.pac_mines

            text = """Normal {}   Super {}      Mines {}   Opponents {}

{}

{}

Commands {}""".format(len(bot.visible_normal_pellets), len(bot.super_pellets),
                      len(bot.living_my_pacs), len(bot.visible_opponent_pacs),
                      '\n'.join((str(pac) if pac.is_alive
                                 else '') for pac in pacs_a),
                      '\n'.join((str(pac) if pac.is_alive
                                 else '') for pac in pacs_b),
                      bot.last_commands)

            self._info_labels[bot_id].config(text=text)

    def _draw_maze(self):
        def draw_cell(pos_x: int, pos_y: int, cell: int):
            canvas_x = pos_x * self._cell_size
            canvas_y = pos_y * self._cell_size
            color = ('#B07070' if cell == botm.Maze.WALL
                     else 'black')

            # Draw wall or empty cell
            self._canvas.create_rectangle(canvas_x, canvas_y,
                                          canvas_x + self._cell_size - 1,
                                          canvas_y + self._cell_size - 1,
                                          outline=color, fill=color)

            # Draw wall added by bots
            for bot in self._game.bots:
                if ((cell != botm.Maze.WALL) and
                        bot._maze.is_wall(botm.Pos(pos_x, pos_y))):
                    color = GameTk.BOT_COLORS[bot.bot_id]
                    self._canvas.create_polygon(
                        canvas_x, canvas_y,
                        canvas_x + (self._cell_size - 1 if bot.bot_id != 0
                                    else 0),
                        canvas_y + (self._cell_size - 1 if bot.bot_id == 0
                                    else 0),
                        canvas_x + self._cell_size - 1,
                        canvas_y + self._cell_size - 1,
                        outline=color, fill=color)

            # Draw pellet
            if cell >= botm.Maze.SUPER_PELLET:
                canvas_x += self._cell_size // 2
                canvas_y += self._cell_size // 2
                radius = (5 if cell == botm.Maze.SUPER_PELLET
                          else 2)
                color = 'yellow'
                self._canvas.create_oval(canvas_x - radius, canvas_y - radius,
                                         canvas_x + radius, canvas_y + radius,
                                         outline=color, fill=color)

        for pos_y in range(self._game.maze.height):
            for pos_x in range(self._game.maze.width):
                draw_cell(pos_x, pos_y,
                          self._game.maze.get_cell(botm.Pos(pos_x, pos_y)))

    def _draw_pacgame(self, pac: gamem.PacGame):
        color = GameTk.BOT_COLORS[pac.bot_id]

        def draw_place(bot_id: int, pos: botm.Pos, radius: int,  # noqa  # pylint: disable=too-many-arguments
                       distance: Optional[int] = None,
                       dash: Optional[Tuple[int, ...]] = None,
                       width: int = 3):
            assert 0 <= bot_id <= 1
            assert radius > 0

            pos_x = pos.x * self._cell_size + self._cell_size // 2
            pos_y = pos.y * self._cell_size + self._cell_size // 2
            if bot_id != 0:
                radius = -radius

            self._canvas.create_line(pos_x - radius, pos_y + radius,
                                     pos_x + radius, pos_y + radius,
                                     fill=color, width=width, dash=dash)
            self._canvas.create_line(pos_x - radius, pos_y - radius,
                                     pos_x - radius, pos_y + radius,
                                     fill=color, width=width, dash=dash)

            radius //= 2
            textcolor = ('silver' if width == 3
                         else 'gray')
            self._canvas.create_text(pos_x, pos_y + radius,
                                     fill=textcolor, text=pac.pac_id)
            if distance is not None:
                self._canvas.create_text(pos_x - radius, pos_y,
                                         fill=textcolor, text=distance)

        pos_x = pac.pos.x * self._cell_size + self._cell_size // 2
        pos_y = pac.pos.y * self._cell_size + self._cell_size // 2

        # Draw destination, next position(s) and path(s)
        if (pac.destination is not None) and (pac.destination != pac.pos):
            radius = self._cell_size // 2 - 1

            # Paths
            if self._draw_paths_checks_tk[pac.bot_id].get():
                paths_poss = self._game.maze.compute_paths_poss(
                    pac.pos, pac.destination)

                for pos in paths_poss:
                    draw_place(pac.bot_id, pos, radius, dash=(3, 5), width=1)

            # Path
            if self._draw_path_checks_tk[pac.bot_id].get():
                path_poss = self._game.maze.compute_path_poss(
                    pac.pos, pac.destination)

                for i, pos in enumerate(path_poss[1:-1]):
                    draw_place(pac.bot_id, pos, radius,
                               distance=i + 1, dash=(3, 5))

            # Destination
            draw_place(pac.bot_id, pac.destination, radius,
                       distance=self._game.maze.distance(pac.pos,
                                                         pac.destination))

        # Draw pac
        radius = self._cell_size // 2 - 3
        self._canvas.create_oval(pos_x - radius, pos_y - radius,
                                 pos_x + radius, pos_y + radius,
                                 outline=color, fill=color)
        self._canvas.create_text(pos_x, pos_y, fill='white', text=pac.pac_id)
        self._canvas.create_text(pos_x, pos_y + 12, fill='silver',
                                 text=('{} {} {}'.format(
                                     botm.Pac.ID_TO_TYPES[pac.type_id][0].lower(),  # noqa
                                     pac.speed_turns_left,
                                     pac.ability_cooldown)))

        # Draw white circle if speed enabled
        radius += 1
        if pac.speed_turns_left > 0:
            self._canvas.create_oval(pos_x - radius, pos_y - radius,
                                     pos_x + radius, pos_y + radius,
                                     outline='white')

        # Draw info text
        if pac.text:
            self._canvas.create_text(pos_x, pos_y - radius, fill='white',
                                     text=pac.text)

    def start(self):
        self._master.mainloop()


#
# Main
######
def main():
    random.seed(666)

    reader = botm.Reader()
    maze = botm.Maze(reader)

    game = GameTk(maze, 5, 2)
    game.start()


if __name__ == '__main__':
    main()
