#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=too-many-lines

"""
botm.

Solution of CodingGame Spring Challenge 2020:
grab the pellets as fast as you can!

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: May 18, 2020
"""

import copy
import itertools
import random
import sys
import time

from typing import Dict, FrozenSet, Iterable, List, Optional, Set, Tuple


#
# Global constant
#################
LOG_LEVEL = 0


#
# Function
##########
def print_log(*data, level: int = 1):
    if level <= LOG_LEVEL:
        print(*data, file=sys.stderr)
        sys.stderr.flush()


#
# Classes
#########
class Bot:  # pylint: disable=too-many-instance-attributes
    """One player, read data, "think" and send commands."""

    MAX_PAC = 5

    def __init__(self, bot_id: int, reader: 'Reader') -> None:
        assert 0 <= bot_id <= 1

        start = time.time()

        self.bot_id = bot_id

        self.reader = reader

        self.duration = 0.0

        self._maze = Maze(reader=reader)
        self._maze.full_normal_pellets()

        self._score = 0
        self._opponent_score = 0

        self._initial_nb_pac = 0
        self.pac_mines = tuple()  # type: Tuple['PacMine', ...]
        self.pac_opponents = tuple()  # type: Tuple['PacOpponent', ...]

        self.living_my_pacs = tuple()  # type: Tuple['PacMine', ...]
        self.visible_opponent_pacs = tuple()  # type: Tuple['PacOpponent', ...]

        self.visible_normal_pellets = tuple()  # type: Tuple['Pos', ...]
        self.super_pellets = tuple()  # type: Tuple['Pos', ...]

        self.last_commands = ''

        self._tour = 0  # ith tour (but not exactly because of speed)

        print_log('Initialized bot {} in {:.2f}ms: maze {}x{}'
                  .format(self.bot_id, (time.time() - start) * 1000,
                          self._maze.width, self._maze.height))

    def _avoid_battle(self, final: bool = False):
        if not self.visible_opponent_pacs:
            return

        all_opponents_poss = set()
        close_opponents_poss = set()
        opponents = dict()
        for opponent in self.visible_opponent_pacs:
            poss = set((opponent.pos, ))
            poss.update(self._maze.possible_moves(opponent.pos))
            close_opponents_poss.update(poss)

            for pos in tuple(poss):
                poss.update(self._maze.possible_moves(pos))

            all_opponents_poss.update(poss)
            opponents[opponent] = poss

        for pac in self.living_my_pacs:  # noqa  # pylint: disable=too-many-nested-blocks
            if pac.destination is not None:
                next_pos = self._maze.compute_next_pos(pac.pos,
                                                       pac.destination)
                for opponent, poss in opponents.items():
                    battle = Pac.battle(pac.type_id, opponent.type_id)
                    if next_pos in poss:
                        print_log('BATTLE', battle, next_pos, pac, opponent)
                    if (battle is not True) and (next_pos in poss):
                        if pac.ability_cooldown == 0:
                            pac.set_switch_other(opponent.type_id)
                            print_log('SWITCH to', pac.type_id)
                        elif final:
                            possible_moves = (
                                set(self._maze.possible_moves(pac.pos))
                                .difference(all_opponents_poss))
                            pac.destination = (possible_moves.pop()
                                               if possible_moves
                                               else None)

                            possible_moves = (
                                set(self._maze.possible_moves(pac.pos))
                                .difference(close_opponents_poss))
                            if possible_moves:
                                pac.destination = possible_moves.pop()

                            print_log('LOST, FINAL', pac.destination)
                        else:
                            print_log('LOST, NOT MOVE')
                            pac.destination = None

                            break

    def _combinations_super_pellets(self):
        distances = tuple(tuple(self._maze.distance(pac.pos, pellet_pos)
                                for pellet_pos in self.super_pellets)
                          for pac in self.living_my_pacs)

        min_nb = min(len(self.living_my_pacs), len(self.super_pellets))
        i_pacs_cs = itertools.combinations(range(len(self.living_my_pacs)),
                                           min_nb)
        j_pellets_ps = itertools.permutations(range(len(self.super_pellets)),
                                              min_nb)

        min_total_distance = sys.maxsize
        min_pair = None
        for i_pacs, j_pellets in itertools.product(i_pacs_cs, j_pellets_ps):
            assert len(i_pacs) == min_nb
            assert len(j_pellets) == min_nb

            total_distance = 0
            for k in range(min_nb):
                row = distances[i_pacs[k]]
                distance = row[j_pellets[k]]
                total_distance += distance  # ** 0.75

            if min_total_distance > total_distance:
                min_total_distance = total_distance
                min_pair = (i_pacs, j_pellets)

        if min_pair is not None:
            i_pacs = min_pair[0]
            j_pellets = min_pair[1]
            for k in range(min_nb):
                pac = self.living_my_pacs[i_pacs[k]]
                move = self.super_pellets[j_pellets[k]]
                pac.set_destination(move)

    def _dispatch_to_direct_battle(self):
        for pac in self.living_my_pacs:
            poss = set(self._possible_moves(pac.pos))
            for opponent in self.pac_opponents:
                if ((pac.battle(pac.type_id, opponent.type_id) is True) and
                        (opponent.pos in poss)):
                    pac.destination = opponent.pos

                    break

    def _dispatch_to_direct_normal_pellet(self):
        assert self.visible_normal_pellets

        for pac in self.living_my_pacs:
            if pac.destination is None:
                poss = tuple(
                    pos for pos in self._possible_moves(pac.pos)
                    if self._maze.get_cell(pos) == Maze.NORMAL_PELLET)
                if poss:
                    pac.set_destination(poss[pac.pac_id % len(poss)])

    def _dispatch_to_direct_super_pellet(self):
        assert self.super_pellets

        for pac in self.living_my_pacs:
            poss = tuple(
                pos for pos in self._possible_moves(pac.pos)
                if self._maze.get_cell(pos) == Maze.SUPER_PELLET)
            if poss:
                pac.set_destination(poss[pac.pac_id % len(poss)])

    def _dispatch_to_closest_maybe_pellets(self):
        keep_maze_rows = copy.deepcopy(self._maze.rows)

        pellets = tuple((self._maze.get_cell(pos), pos)
                        for pos in self._maze.cell_poss
                        if self._maze.get_cell(pos) >= Maze.NORMAL_PELLET)

        previous_path_poss = set()
        for pac in self.living_my_pacs:
            previous_path_poss.update(pac._previous_path[-5:])

        def evaluate(pac: 'PacMine',
                     value: int, pos: 'Pos') -> Tuple[int, int]:
            if pac.pos == pos:
                return (0, 0)

            distance = self._maze.distance(pac.pos, pos)
            evaluation = 1000 / (value * distance ** 2)

            back = set(pac._previous_path[-5:])
            next_pos = self._maze.compute_next_pos(pac.pos, pos)
            if next_pos in previous_path_poss.difference(back):
                evaluation /= 100

            return (round(evaluation), distance)

        for pac in self.living_my_pacs:
            if pac.destination is None:
                pac.destination = max(pellets,
                                      key=lambda t: evaluate(pac, *t))[1]

            if pac.destination is not None:
                for pos in self._maze.compute_path_poss(pac.pos,
                                                        pac.destination):
                    self._maze.set_cell(pos, self._maze.get_cell(pos) + 10)
                print_log(str(self._maze), level=2)

        self._maze.rows = copy.deepcopy(keep_maze_rows)

    def _dispatch_to_closest_normal_pellets(self):
        assert self.visible_normal_pellets

        for pac in self.living_my_pacs:
            if pac.destination is None:
                pac.destination = min(
                    tuple((pac.pos.distance(pos), pos)
                          for pos in self.visible_normal_pellets),
                    key=lambda t: t[0])[1]

    def _dispatch_to_closest_pellets(self):  # noqa  # pylint: disable=too-many-branches
        for pac in self.living_my_pacs:
            pac.destination = None

        if self.super_pellets:
            self._combinations_super_pellets()

        if self.visible_normal_pellets:
            self._dispatch_to_direct_normal_pellet()

        self._avoid_battle()

        self._dispatch_to_closest_maybe_pellets()

        self._avoid_battle()

        if self.visible_normal_pellets:
            self._dispatch_to_closest_normal_pellets()

        self._avoid_battle()

        for pac in self.living_my_pacs:
            if (pac.destination is None) or (pac.destination == pac.pos):
                pac.destination = self._random_move(pac.pos)

        if self.super_pellets:
            self._dispatch_to_direct_super_pellet()

        self._dispatch_to_direct_battle()

    def _is_first_tour(self):
        return self._tour == 1

    def _clear_cells(self):
        for pac in self.living_my_pacs:
            self._clear_cells_pos(pac.pos)

    def _clear_cells_pos(self, pos: 'Pos'):
        visible_pellets = frozenset(
            self.visible_normal_pellets).union(self.super_pellets)

        # Horizontal
        for sign in (-1, 1):
            for i in range(self._maze.width):
                current_pos = Pos((pos.x + sign * i) % self._maze.width, pos.y)
                if self._maze.is_wall(current_pos):
                    break

                if ((self._maze.get_cell(current_pos) >= Maze.SUPER_PELLET) and
                        (current_pos not in visible_pellets)):
                    self._maze.clear_cell(current_pos)

        # Vertical
        for sign in (-1, 1):
            for i in range(self._maze.height):
                current_pos = Pos(pos.x,
                                  (pos.y + sign * i) % self._maze.height)
                if self._maze.is_wall(current_pos):
                    break

                if ((self._maze.get_cell(current_pos) >= Maze.SUPER_PELLET) and
                        (current_pos not in visible_pellets)):
                    self._maze.clear_cell(current_pos)

    def _move(self) -> str:
        pac_poss = frozenset(pac.pos for pac in self.living_my_pacs)
        for pac in self.living_my_pacs:
            self._maze.close_dead_end(pac.pos, keep_poss=pac_poss)

        self._dispatch_to_closest_pellets()
        self._avoid_battle(final=True)

        for pac in self.living_my_pacs:
            if pac.blocked >= 2:
                pac.set_destination(self._random_move(pac.pos))

            if (pac.speed_turns_left == 0) and (pac.ability_cooldown == 0):
                pac.set_speed()

        self.last_commands = '|'.join(pac.command()
                                      for pac in self.living_my_pacs)
        print(self.last_commands)
        sys.stdout.flush()

        return self.last_commands

    def _possible_moves(self, pos: 'Pos') -> Tuple['Pos', ...]:
        moves = (set(self._maze.possible_moves(pos))
                 .difference(pac.pos for pac in self.living_my_pacs)
                 .difference(pac.pos for pac in self.visible_opponent_pacs))

        return tuple(moves)

    def _random_move(self, pos: 'Pos') -> Optional['Pos']:
        possible_moves = self._possible_moves(pos)

        print_log('RANDOM', pos)

        return (random.choice(possible_moves) if possible_moves
                else None)

    def _read_pacs(self):  # pylint: disable=too-many-branches
        # Read
        # # Number of my pacs and visible opponents pacs
        nb_visible_pac = int(self.reader.read_line())

        # # All datas
        if self._initial_nb_pac == 0:
            self._initial_nb_pac = Bot.MAX_PAC
        data_mines = [None] * self._initial_nb_pac
        data_opponents = [None] * self._initial_nb_pac
        for _ in range(nb_visible_pac):
            (pac_id_str, is_mine_str, pos_x_str, pos_y_str, type_id_str,
             speed_turns_left_str,
             ability_cooldown_str) = self.reader.read_line().split()

            pac_id = int(pac_id_str)
            data = ((Pos(int(pos_x_str), int(pos_y_str)),
                     Pac.EXTENDED_TYPE_TO_IDS[type_id_str],
                     int(speed_turns_left_str),
                     int(ability_cooldown_str)))

            if is_mine_str != '0':  # mine pac
                data_mines[pac_id] = data
            else:                   # opponent pac
                data_opponents[pac_id] = data

        if self._is_first_tour():  # create all pacs
            self._initial_nb_pac = sum(1 for data in data_mines
                                       if data is not None)
            data_mines = data_mines[:self._initial_nb_pac]
            data_opponents = data_opponents[:self._initial_nb_pac]
            self.pac_mines = tuple(
                PacMine(0, pac_id, Pos(0, 0), Pac.ROCK, 0, 0)
                for pac_id in range(self._initial_nb_pac))
            self.pac_opponents = tuple(
                PacOpponent(1, pac_id, Pos(0, 0), Pac.ROCK, 0, 0)
                for pac_id in range(self._initial_nb_pac))

        # Update mine pac objects
        for pac_id, data in enumerate(data_mines):
            pac = self.pac_mines[pac_id]
            if (data is None) or (data[1] == Pac.DEAD):
                print_log(pac_id, data)
                pac.is_alive = False
            else:
                pac._update(*data)

            pac._previous_path.append(pac.pos)
            print_log(pac, 'previous', pac._previous_path, level=2)

        self.living_my_pacs = tuple(pac for pac in self.pac_mines
                                    if pac.is_alive)

        # Update opponent pac objects
        for pac_id, data in enumerate(data_opponents):
            if data is None:
                if self._is_first_tour():  # create symmetrical
                    pac = self.pac_mines[pac_id]
                    self.pac_opponents[pac_id]._update(
                        self._maze.symmetrical(pac.pos),
                        pac.type_id, pac.speed_turns_left,
                        pac.ability_cooldown)

                self.pac_opponents[pac_id].is_visible = False
            elif data[1] == Pac.DEAD:
                self.pac_opponents[pac_id].is_alive = False
            else:
                self.pac_opponents[pac_id]._update(*data)
                self.pac_opponents[pac_id].is_visible = True

        self.visible_opponent_pacs = tuple(pac for pac in self.pac_opponents
                                           if pac.is_visible and pac.is_alive)

    def _read_pellets(self):
        visible_pellet_count = int(self.reader.read_line())

        pellets = []
        super_pellets = []
        for _ in range(visible_pellet_count):
            # value: amount of points this pellet is worth
            pos_x, pos_y, value = (int(j)
                                   for j in self.reader.read_line().split())
            pos = Pos(pos_x, pos_y)
            if value == 1:  # normal pellet
                pellets.append(pos)
                self._maze.set_cell(pos, Maze.NORMAL_PELLET)
            else:           # super pellet
                assert value == Maze.PELLET_VALUES[Maze.SUPER_PELLET], value

                super_pellets.append(pos)
                self._maze.set_cell(pos, Maze.SUPER_PELLET)

        self.visible_normal_pellets = tuple(pellets)
        self.super_pellets = tuple(super_pellets)

    def _read_score(self):
        self._score, self._opponent_score = (
            int(i) for i in self.reader.read_line().split())

    def _read_tour(self):
        self._tour += 1

        self._read_score()
        self._read_pacs()
        if not self._is_first_tour():
            self._maze.increase_maybe_pellets()
        self._read_pellets()

        self._clear_cells()

    def play(self, nb_tour: Optional[int] = None) -> str:
        commands = ''

        while (nb_tour is None) or (nb_tour > 0):
            if nb_tour is not None:
                nb_tour -= 1

            start = time.time()

            self._read_tour()
            commands = self._move()

            self.duration = (time.time() - start) * 1000

            print_log(str(self._maze), level=2)
            print_log('{0} {1}: {2:.2f}ms Score {3}-{4} {0}\n'
                      .format('-' * 20,
                              self._tour, self.duration,
                              self._score, self._opponent_score), level=2)

        return commands

    def visible_poss(self) -> FrozenSet['Pos']:
        poss = set()  # type: Set['Pos']
        for pac in self.living_my_pacs:
            poss.update(self._maze.visible_poss(pac.pos))

        return frozenset(poss)


class Maze:  # noqa  # pylint: disable=too-many-public-methods,too-many-instance-attributes
    """
    Maze represented as an array,
    with precomputed minimal paths between positions.
    """

    WALL = -2
    EMPTY = -1
    SUPER_PELLET = 0
    NORMAL_PELLET = 1
    MAYBE_PELLET = 2  # and greater

    _CELL_TO_SYMBOLS = {WALL: '#',
                        EMPTY: ' ',
                        SUPER_PELLET: 'o',
                        NORMAL_PELLET: '.'}

    _SYMBOL_TO_CELLS = {'#': WALL,
                        ' ': EMPTY,
                        'o': SUPER_PELLET,
                        '.': NORMAL_PELLET}

    _NEXT_POS = dict()  # type: Dict[Tuple['Pos', 'Pos'], 'Pos']

    PELLET_VALUES = (10, 1)

    def __init__(self, reader: 'Reader') -> None:
        self.rows = tuple()  # type: Tuple[List[int], ...]
        self.cell_poss = frozenset()  # type: FrozenSet['Pos']

        self._reader = reader
        self._read(reader)

        assert self.rows

        self.width = len(self.rows[0])
        self.height = len(self.rows)

        self._paths_rows = tuple()  # type: Tuple[Tuple[Tuple[FrozenSet['Pos'], ...], ...], ...]  # noqa
        self._compute_paths()

        self._distances = dict()  # type: Dict[Tuple['Pos', 'Pos'], int]

    def __deepcopy__(self, memo: Dict) -> 'Maze':
        maze = copy.copy(self)
        maze.rows = copy.deepcopy(self.rows)

        return maze

    def __repr__(self) -> str:
        return '{}("""{} {}\n{}""")'.format(self.__class__.__name__,
                                            self.width, self.height, str(self))

    def __str__(self) -> str:
        return '\n'.join(''.join(Maze._CELL_TO_SYMBOLS.get(cell,
                                                           str(cell % 10))
                                 for cell in row)
                         for row in self.rows)

    def _compute_paths(self):  # pylint: disable=too-many-locals
        empty_set = frozenset()
        paths_rows = [[[empty_set]] * self.width for _ in range(self.height)]
        already_rows = [[set() for _ in range(self.width)]
                        for _ in range(self.height)]

        all_pos = []
        for pos_y, row in enumerate(self.rows):
            for pos_x, cell in enumerate(row):
                if cell != Maze.WALL:
                    pos = Pos(pos_x, pos_y)
                    already_rows[pos_y][pos_x].add(pos)
                    all_pos.append(pos)
                    moves = set(self.possible_moves(pos))
                    if not moves:
                        moves = empty_set

                    assert moves

                    paths_rows[pos_y][pos_x] = [frozenset(moves)]

        self.all_pos = tuple(all_pos)

        for dist in range(0, self.width * self.height):
            updated = False
            for pos in all_pos:
                paths = paths_rows[pos.y][pos.x]
                if (dist < len(paths)) and paths[dist]:
                    already_rows[pos.y][pos.x].update(paths[dist])
                    moves = set()
                    for move in paths[dist]:
                        moves.update(self.possible_moves(move))
                    moves.difference_update(already_rows[pos.y][pos.x])
                    if moves:
                        paths.append(frozenset(moves))
                        updated = True
                    else:
                        paths.append(empty_set)

            if not updated:
                break

        for i, row in enumerate(paths_rows):
            paths_rows[i] = tuple(tuple(moves) for moves in row)

        self._paths_rows = tuple(paths_rows)  # type: Tuple[Tuple[Tuple[FrozenSet['Pos'], ...], ...], ...]  # noqa

    def _read(self, reader: 'Reader'):
        """
        width: size of the grid
        height: top left corner is (x=0, y=0)
        One line of the grid: space ' ' is floor, pound '#' is wall
        """
        width, height = (int(i) for i in reader.read_line().split())

        cell_poss = []
        rows = []
        for pos_y in range(height):
            line = reader.read_line()
            assert len(line) == width

            row = list(
                Maze._SYMBOL_TO_CELLS.get(symbol,
                                          (-666
                                           if symbol in Maze._SYMBOL_TO_CELLS
                                           else int(symbol)))
                for symbol in line)
            rows.append(row)

            for pos_x, cell in enumerate(row):
                if cell != Maze.WALL:
                    cell_poss.append(Pos(pos_x, pos_y))

        self.cell_poss = frozenset(cell_poss)
        self.rows = tuple(rows)

    def clear_cell(self, pos: 'Pos'):
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height
        assert self.rows[pos.y][pos.x] >= Maze.EMPTY

        self.set_cell(pos, Maze.EMPTY)

    def close_dead_end(self, pos: 'Pos',
                       keep_poss: FrozenSet['Pos'] = frozenset()):
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height
        assert not self.is_wall(pos)

        self.close_up_dead_end(pos, keep_poss=keep_poss)
        self.close_down_dead_end(pos, keep_poss=keep_poss)
        self.close_left_dead_end(pos, keep_poss=keep_poss)
        self.close_right_dead_end(pos, keep_poss=keep_poss)

    def close_down_dead_end(self, pos: 'Pos',
                            keep_poss: FrozenSet['Pos'] = frozenset()):
        current_pos = self.search_down_wall(pos)
        if current_pos is None:
            return

        while True:
            current_pos = Pos(current_pos.x, (current_pos.y - 1) % self.height)
            if ((self.get_cell(current_pos) >= Maze.SUPER_PELLET) or
                    (current_pos == pos) or
                    (current_pos in keep_poss) or
                    len(self.possible_moves(current_pos)) > 1):
                break

            self.set_wall(current_pos)

    def close_left_dead_end(self, pos: 'Pos',
                            keep_poss: FrozenSet['Pos'] = frozenset()):
        current_pos = self.search_left_wall(pos)
        if current_pos is None:
            return

        while True:
            current_pos = Pos((current_pos.x + 1) % self.width, current_pos.y)
            if ((self.get_cell(current_pos) >= Maze.SUPER_PELLET) or
                    (current_pos == pos) or
                    (current_pos in keep_poss) or
                    len(self.possible_moves(current_pos)) > 1):
                break

            self.set_wall(current_pos)

    def close_right_dead_end(self, pos: 'Pos',
                             keep_poss: FrozenSet['Pos'] = frozenset()):
        current_pos = self.search_right_wall(pos)
        if current_pos is None:
            return

        while True:
            current_pos = Pos((current_pos.x - 1) % self.width, current_pos.y)
            if ((self.get_cell(current_pos) >= Maze.SUPER_PELLET) or
                    (current_pos == pos) or
                    (current_pos in keep_poss) or
                    len(self.possible_moves(current_pos)) > 1):
                break

            self.set_wall(current_pos)

    def close_up_dead_end(self, pos: 'Pos',
                          keep_poss: FrozenSet['Pos'] = frozenset()):
        current_pos = self.search_up_wall(pos)
        if current_pos is None:
            return

        while True:
            current_pos = Pos(current_pos.x, (current_pos.y + 1) % self.height)
            if ((self.get_cell(current_pos) >= Maze.SUPER_PELLET) or
                    (current_pos == pos) or
                    (current_pos in keep_poss) or
                    len(self.possible_moves(current_pos)) > 1):
                break

            self.set_wall(current_pos)

    def compute_next_pos(self, pos_from: 'Pos', pos_to: 'Pos') -> 'Pos':
        assert 0 <= pos_from.x < self.width
        assert 0 <= pos_from.y < self.height
        assert 0 <= pos_to.x < self.width
        assert 0 <= pos_to.y < self.height
        assert pos_from != pos_to

        key = (pos_from, pos_to)
        next_pos = self._NEXT_POS.get(key)
        if next_pos is None:
            next_pos = random.choice(
                tuple(self.compute_next_poss(pos_from, pos_to)))
            self._NEXT_POS[key] = next_pos

        return next_pos

    def compute_next_poss(self, pos_from: 'Pos', pos_to: 'Pos') -> FrozenSet['Pos']:  # noqa
        assert 0 <= pos_from.x < self.width
        assert 0 <= pos_from.y < self.height
        assert 0 <= pos_to.x < self.width
        assert 0 <= pos_to.y < self.height
        assert pos_from != pos_to

        distance1 = self.distance(pos_from, pos_to) - 1

        return frozenset(pos for pos in self.paths(pos_from)[0]
                         if (not self.is_wall(pos) and
                             not self.is_wall(pos_to) and
                             (self.distance(pos, pos_to) == distance1)))

    def compute_path_poss(self, pos_from: 'Pos', pos_to: 'Pos') -> Tuple['Pos', ...]:  # noqa
        assert 0 <= pos_from.x < self.width
        assert 0 <= pos_from.y < self.height
        assert 0 <= pos_to.x < self.width
        assert 0 <= pos_to.y < self.height
        assert pos_from != pos_to

        path_poss = [pos_from]
        while pos_from != pos_to:
            pos_from = self.compute_next_pos(pos_from, pos_to)
            path_poss.append(pos_from)

        return tuple(path_poss)

    def compute_paths_poss(self, pos_from: 'Pos', pos_to: 'Pos') -> FrozenSet['Pos']:  # noqa
        assert 0 <= pos_from.x < self.width
        assert 0 <= pos_from.y < self.height
        assert 0 <= pos_to.x < self.width
        assert 0 <= pos_to.y < self.height
        assert pos_from != pos_to

        paths_poss = set()
        poss = set((pos_from, ))
        while poss:
            paths_poss.update(poss)

            next_poss = set()  # type: Set[Pos]
            for pos in poss:
                next_poss.update(self.compute_next_poss(pos, pos_to))

            next_poss.difference_update(paths_poss)
            if pos_from in next_poss:
                next_poss.remove(pos_from)
            if pos_to in next_poss:
                next_poss.remove(pos_to)

            paths_poss.update(next_poss)
            poss = next_poss

        paths_poss.add(pos_to)

        return frozenset(paths_poss)

    def distance(self, pos_a: 'Pos', pos_b: 'Pos') -> int:
        assert 0 <= pos_a.x < self.width
        assert 0 <= pos_a.y < self.height
        assert 0 <= pos_b.x < self.width
        assert 0 <= pos_b.y < self.height
        assert not self.is_wall(pos_a)
        assert not self.is_wall(pos_b)

        if pos_a == pos_b:
            return 0

        # Use symmetry to reduce cache
        if ((self.width - 1 - pos_a.x <= pos_a.x) and
                (self.width - 1 - pos_b.x <= pos_b.x)):
            pos_a = Pos(self.width - 1 - pos_a.x, pos_a.y)
            pos_b = Pos(self.width - 1 - pos_b.x, pos_b.y)

        # Sort to reduce cache
        if pos_b < pos_a:
            pos_a, pos_b = pos_b, pos_a

        # Search in cache
        result = self._distances.get((pos_a, pos_b))
        if result is not None:
            return result

        result = sys.maxsize

        # Compute
        for dist, positions in enumerate(self.paths(pos_a)):
            if pos_b in positions:
                result = dist + 1

                break

        assert result < sys.maxsize

        self._distances[(pos_a, pos_b)] = result

        return result

    def full_normal_pellets(self):
        for row in self.rows:
            for pos_x, cell in enumerate(row):
                if cell == Maze.EMPTY:
                    row[pos_x] = Maze.NORMAL_PELLET

    def get_cell(self, pos: 'Pos') -> int:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self.rows[pos.y][pos.x]

    def get_maybe_pellet(self, pos: 'Pos') -> Optional[int]:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        cell = self.get_cell(pos)

        return (cell if cell >= Maze.SUPER_PELLET
                else None)

    def increase_maybe_pellets(self):
        for row in self.rows:
            for pos_x, cell in enumerate(row):
                if cell >= Maze.NORMAL_PELLET:
                    row[pos_x] += 1

    def is_certainly_empty(self, pos: 'Pos') -> bool:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self.get_cell(pos) == Maze.EMPTY

    def is_maybe_pellet(self, pos: 'Pos') -> bool:
        """Return True if super or normal pellet or maybe there is a pellet."""
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self.get_cell(pos) >= Maze.SUPER_PELLET

    def is_normal_pellet(self, pos: 'Pos') -> bool:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self.get_cell(pos) == Maze.NORMAL_PELLET

    def is_super_pellet(self, pos: 'Pos') -> bool:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self.get_cell(pos) == Maze.SUPER_PELLET

    def is_wall(self, pos: 'Pos') -> bool:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self.get_cell(pos) == Maze.WALL

    def paths(self, pos: 'Pos') -> Tuple[FrozenSet['Pos'], ...]:
        """
        Return an ordered sequence of sets of positions reachable from pos.

        The first set is reachable in one movement,
        the second set in two movements, etc.

        All cells reachable in n movement and pos are not repeated after.
        """
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return self._paths_rows[pos.y][pos.x]

    def possible_moves(self, pos: 'Pos') -> Tuple['Pos', ...]:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height
        # assert self.rows[pos.y][pos.x] != Maze.WALL

        moves = []

        def add(pos: 'Pos'):
            if not self.is_wall(pos):
                moves.append(pos)

        add(Pos((pos.x + 1) % self.width, pos.y))
        add(Pos((pos.x - 1) % self.width, pos.y))
        add(Pos(pos.x, (pos.y + 1) % self.height))
        add(Pos(pos.x, (pos.y - 1) % self.height))

        return tuple(moves)

    def set_cell(self, pos: 'Pos', cell: int):
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height
        assert self.rows[pos.y][pos.x] != Maze.WALL
        assert cell >= Maze.EMPTY, cell

        self.rows[pos.y][pos.x] = cell

    def set_wall(self, pos: 'Pos'):
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height
        assert self.rows[pos.y][pos.x] != Maze.WALL

        self.rows[pos.y][pos.x] = Maze.WALL
        self._paths_rows = tuple(tuple(self._paths_rows[j][i]
                                       for i in range(self.width))
                                 for j in range(self.height))
        self.cell_poss = self.cell_poss.difference((pos, ))

    def search_down_wall(self, pos: 'Pos') -> Optional['Pos']:
        wall_pos = pos
        while not self.is_wall(wall_pos):
            wall_pos = Pos(wall_pos.x, (wall_pos.y + 1) % self.height)
            if wall_pos == pos:
                return None

        return wall_pos

    def search_left_wall(self, pos: 'Pos') -> Optional['Pos']:
        wall_pos = pos
        while not self.is_wall(wall_pos):
            wall_pos = Pos((wall_pos.x - 1) % self.width, wall_pos.y)
            if wall_pos == pos:
                return None

        return wall_pos

    def search_right_wall(self, pos: 'Pos') -> Optional['Pos']:
        wall_pos = pos
        while not self.is_wall(wall_pos):
            wall_pos = Pos((wall_pos.x + 1) % self.width, wall_pos.y)
            if wall_pos == pos:
                return None

        return wall_pos

    def search_up_wall(self, pos: 'Pos') -> Optional['Pos']:
        wall_pos = pos
        while not self.is_wall(wall_pos):
            wall_pos = Pos(wall_pos.x, (wall_pos.y - 1) % self.height)
            if wall_pos == pos:
                return None

        return wall_pos

    def symmetrical(self, pos: 'Pos') -> 'Pos':
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height

        return pos.symmetrical(self.width)

    def to_str(self, marks: Optional[Dict['Pos', str]] = None) -> str:
        seq = []
        for pos_y, row in enumerate(self.rows):
            line = []
            for pos_x, cell in enumerate(row):
                symbol = (None if marks is None
                          else marks.get(Pos(pos_x, pos_y)))

                assert (symbol is None) or (cell != Maze.WALL)

                if symbol is None:
                    symbol = Maze._CELL_TO_SYMBOLS.get(cell, ' ')

                assert isinstance(symbol, str)
                assert len(symbol) == 1

                line.append(symbol)

            seq.append(''.join(line))

        return '\n'.join(seq)

    def visible_poss(self, pos: 'Pos') -> FrozenSet['Pos']:
        assert 0 <= pos.x < self.width
        assert 0 <= pos.y < self.height
        assert self.rows[pos.y][pos.x] != Maze.WALL

        poss = set((pos, ))

        for sign in (-1, 1):
            for i in range(1, self.width):
                current_pos = Pos((pos.x + sign * i) % self.width, pos.y)
                if self.is_wall(current_pos) or (current_pos in poss):
                    break
                poss.add(current_pos)

        for sign in (-1, 1):
            for i in range(1, self.height):
                current_pos = Pos(pos.x, (pos.y + sign * i) % self.height)
                if self.is_wall(current_pos) or (current_pos in poss):
                    break
                poss.add(current_pos)

        poss.remove(pos)

        return frozenset(poss)


class Pac:  # pylint: disable=too-many-instance-attributes
    """Abstract class to represent common data and behavior of pac."""

    ROCK = 0
    PAPER = 1
    SCISSORS = 2
    DEAD = 3

    ID_TO_TYPES = ('ROCK', 'PAPER', 'SCISSORS')
    TYPE_TO_IDS = {'ROCK': ROCK,
                   'PAPER': PAPER,
                   'SCISSORS': SCISSORS}

    EXTENDED_TYPE_TO_IDS = {'ROCK': ROCK,
                            'PAPER': PAPER,
                            'SCISSORS': SCISSORS,
                            'DEAD': DEAD}

    @staticmethod
    def battle(a_type_id: int, b_type_id: int) -> Optional[bool]:
        """
        Return True if a wins against b,
        None if tie,
        False if a looses.

        0 ROCK wins against     2 SCISSORS
        1 PAPER wins against    0 ROCK
        2 SCISSORS wins against 1 PAPER
        """
        assert Pac.ROCK <= a_type_id <= Pac.SCISSORS
        assert Pac.ROCK <= b_type_id <= Pac.SCISSORS

        return (None if a_type_id == b_type_id
                else a_type_id == (b_type_id + 1) % 3)

    def __init__(self, bot_id: int,  # pylint: disable=too-many-arguments
                 pac_id: int, pos: 'Pos', type_id: int,
                 speed_turns_left: int, ability_cooldown: int) -> None:
        assert 0 <= bot_id <= 1
        assert pac_id >= 0
        assert Pac.ROCK <= type_id <= Pac.SCISSORS

        if type(self) is Pac:  # pylint: disable=unidiomatic-typecheck
            raise Exception('Pac is an abstract class, you must instantiate a subclass!')  # noqa

        self.bot_id = bot_id
        self.pac_id = pac_id
        self.pos = pos
        self.type_id = type_id
        self.speed_turns_left = speed_turns_left
        self.ability_cooldown = ability_cooldown

        self.is_alive = True
        self.previous_pos = pos  # previous visible position

    def __repr__(self):
        return ('{}({}, {}, "{}", {}, {})'
                .format(self.__class__.__name__,
                        self.pac_id, repr(self.pos),
                        Pac.ID_TO_TYPES[self.type_id],
                        self.speed_turns_left, self.ability_cooldown))

    def __str__(self):
        return ('{} {} ({})->({}) {} speed {} ability {}'
                .format(self.__class__.__name__,
                        self.pac_id, self.previous_pos, self.pos,
                        Pac.ID_TO_TYPES[self.type_id],
                        self.speed_turns_left, self.ability_cooldown))

    def _update(self, pos: 'Pos', type_id: int,
                speed_turns_left: int, ability_cooldown: int) -> None:
        self.previous_pos = self.pos

        self.pos = pos
        self.type_id = type_id
        self.speed_turns_left = speed_turns_left
        self.ability_cooldown = ability_cooldown

    def battle_looser(self, other: 'Pac') -> Optional['Pac']:
        winner = Pac.battle(self.type_id, other.type_id)

        return (None if winner is None
                else (other if winner
                      else self))

    def battle_winner(self, other: 'Pac') -> Optional['Pac']:
        winner = Pac.battle(self.type_id, other.type_id)

        return (None if winner is None
                else (self if winner
                      else other))


class PacMine(Pac):
    """My pac."""

    def __init__(self, bot_id: int,  # pylint: disable=too-many-arguments
                 pac_id: int, pos: 'Pos', type_id: int,
                 speed_turns_left: int, ability_cooldown: int) -> None:
        assert 0 <= bot_id <= 1
        assert pac_id >= 0
        assert Pac.ROCK <= type_id <= Pac.SCISSORS
        assert speed_turns_left >= 0
        assert ability_cooldown >= 0

        super().__init__(bot_id,
                         pac_id, pos, type_id,
                         speed_turns_left, ability_cooldown)

        self.destination = None  # type: Optional['Pos']
        self.blocked = 0

        self.to_speed = False
        self.to_switch = None  # type: Optional[int]

        self._previous_path = []  # type: List['Pos']

    def __str__(self):
        return ('{} destination ({}) blocked {} to_speed {} to_switch {}'
                .format(super().__str__(), self.destination,
                        self.blocked, self.to_speed, self.to_switch))

    def _update(self, pos: 'Pos', type_id: int,
                speed_turns_left: int, ability_cooldown: int) -> None:
        super()._update(pos, type_id, speed_turns_left, ability_cooldown)

        if self.destination is not None and (self.previous_pos == self.pos):
            self.blocked += 1
        else:
            self.blocked = 0

        if self.destination == self.pos:
            self.destination = None

        if self.to_switch == self.type_id:
            self.to_switch = None

    def command(self) -> str:
        if self.to_switch is not None:
            to_switch = self.to_switch
            self.to_switch = None
            if to_switch != self.type_id:
                return 'SWITCH {} {}'.format(self.pac_id,
                                             Pac.ID_TO_TYPES[to_switch])
            self.blocked = -1

        if self.to_speed:
            self.to_speed = False
            self.blocked = -1

            return 'SPEED {}'.format(self.pac_id)

        if self.destination == self.pos:
            self.destination = None
        elif self.destination is not None:
            return 'MOVE {0} {1} {1}'.format(self.pac_id,
                                             str(self.destination))

        return ''

    def set_destination(self, pos: Optional['Pos']):
        self.destination = (None
                            if pos == self.pos
                            else pos)

    def set_speed(self):
        self.to_speed = True

    def set_switch(self, type_id: int):
        assert Pac.ROCK <= type_id <= Pac.SCISSORS

        self.to_switch = (None
                          if type_id == self.type_id
                          else type_id)

    def set_switch_other(self, avoid_type_id: int):
        assert Pac.ROCK <= avoid_type_id <= Pac.SCISSORS

        type_ids = set((Pac.ROCK, Pac.PAPER,
                        Pac.SCISSORS)).difference((self.type_id,
                                                   avoid_type_id))

        self.to_switch = type_ids.pop()

    def type(self) -> str:
        return Pac.ID_TO_TYPES[self.type_id]


class PacOpponent(Pac):
    """Opponent pac."""

    def __init__(self, bot_id: int,  # pylint: disable=too-many-arguments
                 pac_id: int, pos: 'Pos', type_id: int,
                 speed_turns_left: int, ability_cooldown: int) -> None:
        assert 0 <= bot_id <= 1
        assert pac_id >= 0
        assert Pac.ROCK <= type_id <= Pac.SCISSORS
        assert speed_turns_left >= 0
        assert ability_cooldown >= 0

        super().__init__(bot_id,
                         pac_id, pos, type_id,
                         speed_turns_left, ability_cooldown)

        self.is_visible = True

    def __str__(self):
        return (super().__str__() if self.is_visible
                else '{} {} ({})->hidden'.format(self.__class__.__name__,
                                                 self.pac_id,
                                                 self.previous_pos))


class Pos:
    """Position."""

    def __init__(self, x: int, y: int) -> None:
        assert x >= 0, x
        assert y >= 0, y

        self.x = x  # pylint: disable=invalid-name
        self.y = y  # pylint: disable=invalid-name

        self._hash = hash((x, y))

    def __eq__(self, other: Optional['Pos']) -> bool:  # type: ignore
        return (other is not None and
                (self.x == other.x) and (self.y == other.y))

    def __lt__(self, other) -> bool:
        return (self.y, self.x) < (other.y, other.x)

    def __hash__(self) -> int:
        return self._hash

    def __repr__(self):
        return '{}({}, {})'.format(self.__class__.__name__, self.x, self.y)

    def __str__(self):
        return '{} {}'.format(self.x, self.y)

    def distance(self, other: 'Pos') -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)

    def symmetrical(self, width: int) -> 'Pos':
        assert width > 0

        return Pos(width - 1 - self.x, self.y)


class Reader:
    """To read data from stdin or from list of str alimented by gamem.Game."""

    def __init__(self, lines: Optional[Iterable[str]] = None) -> None:
        self._lines = (None if lines is None
                       else list(lines))

    def add_line(self, line: str):
        if self._lines is None:
            self._lines = [line]
        else:
            self._lines.append(line)

    def add_lines(self, lines: Iterable[str]):
        if self._lines is None:
            self._lines = list(lines)
        else:
            self._lines.extend(lines)

    def read_line(self) -> str:
        return (sys.stdin.readline().rstrip('\n') if self._lines is None
                else self._lines.pop(0))


#
# Main
######
def main():
    nb_tour = (int(sys.argv[1]) if len(sys.argv) >= 2
               else None)

    reader = Reader()
    bot = Bot(0, reader)
    bot.play(nb_tour)


if __name__ == '__main__':
    main()
