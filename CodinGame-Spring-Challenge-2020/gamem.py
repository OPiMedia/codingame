#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
gamem.

(Very imperfect) engine to test solution of CodingGame Spring Challenge 2020.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: May 18, 2020
"""

import copy
import random
import sys

from typing import Callable, FrozenSet, List, Optional, Set, Tuple

import botm


#
# Global variables
##################
HISTORY = []

HISTORY_DISPLAY = []


#
# Classes
#########
class Game:  # pylint: disable=too-many-instance-attributes
    def __init__(self, maze: botm.Maze,
                 nb_pac_by_bot: int, nb_super_pellet_by_symmetry: int,
                 nb_tour: int = 200):
        assert nb_pac_by_bot > 0
        assert nb_super_pellet_by_symmetry >= 0
        assert nb_tour >= 0

        self.maze = maze
        self._nb_pac_by_bot = nb_pac_by_bot
        self.nb_tour = nb_tour

        self.tour = 0
        self.scores = [0] * 2

        self.display_function = None  # type: Optional[Callable[[], None]]

        # Init bots
        bots = []

        for bot_id in range(2):
            reader = botm.Reader()
            reader.add_lines(('{} {}'.format(maze.width, maze.height), ))
            reader.add_lines(str(maze).split('\n'))
            bots.append(botm.Bot(bot_id, reader))

        self.bots = tuple(bots)

        # Init or reinit maze
        self._init_maze(nb_super_pellet_by_symmetry)

        # Init pacs
        self.pacs = (([], []))  # type: Tuple[List[PacGame], List[PacGame]]
        self._init_pacs(self._nb_pac_by_bot)

        # Save initial configuration
        HISTORY.append(copy.deepcopy(self))
        HISTORY_DISPLAY.append(copy.deepcopy(self))

    def __str__(self):
        maze = list(list(symbol for symbol in line)
                    for line in str(self.maze).split('\n'))

        for bot in self.bots:
            for pac in self.pacs[bot.bot_id]:
                maze[pac.pos.y][pac.pos.x] = ('X' if bot.bot_id == 0
                                              else 'x')

        return '\n'.join(''.join(line) for line in maze)

    def _get_saved_tour(self, tour: int):
        assert 0 <= tour < len(HISTORY)

        self.tour = tour
        self.scores = copy.deepcopy(HISTORY[tour].scores)
        self.bots = copy.deepcopy(HISTORY[tour].bots)
        self.maze = copy.deepcopy(HISTORY[tour].maze)
        self.pacs = copy.deepcopy(HISTORY[tour].pacs)

    def _get_saved_tour_display(self, tour: int):
        assert 0 <= tour < len(HISTORY_DISPLAY)

        self.tour = tour
        self.scores = copy.deepcopy(HISTORY_DISPLAY[tour].scores)
        self.bots = copy.deepcopy(HISTORY_DISPLAY[tour].bots)
        self.maze = copy.deepcopy(HISTORY_DISPLAY[tour].maze)
        self.pacs = copy.deepcopy(HISTORY_DISPLAY[tour].pacs)

    def _init_maze(self, nb_super_pellet_by_symmetry: int):
        assert nb_super_pellet_by_symmetry >= 0

        # All cells that are not wall
        poss = list(botm.Pos(x, y)
                    for y in range(self.maze.height)
                    for x in range(self.maze.width)
                    if not self.maze.is_wall(botm.Pos(x, y)))

        # Set normal pellets
        for pos in poss:
            self.maze.set_cell(pos, botm.Maze.NORMAL_PELLET)

        # Set super pellets
        if nb_super_pellet_by_symmetry > 0:
            random.shuffle(poss)
            already = set()  # type: Set[botm.Pos]
            for pos in poss:
                pos_sym = self.maze.symmetrical(pos)
                if (pos not in already) and (pos_sym not in already):
                    already.update((pos, pos_sym))
                    self.maze.set_cell(pos, botm.Maze.SUPER_PELLET)
                    self.maze.set_cell(pos_sym, botm.Maze.SUPER_PELLET)
                    nb_super_pellet_by_symmetry -= 1
                    if nb_super_pellet_by_symmetry == 0:
                        break

    def _init_pacs(self, nb_pac_by_bot: int):
        assert nb_pac_by_bot > 0

        # All cells that are normal pellet, without symmetrical doubloons
        set_poss = set()
        for pos_y in range(self.maze.height):
            for pos_x in range(self.maze.width):
                pos = botm.Pos(pos_x, pos_y)
                if (self.maze.is_normal_pellet(pos) and
                        (self.maze.symmetrical(pos) != pos) and
                        (self.maze.symmetrical(pos) not in set_poss)):
                    set_poss.add(pos)

        poss = list(set_poss)
        random.shuffle(poss)

        del set_poss

        # Init pacs
        type_ids = (botm.Pac.ROCK, botm.Pac.PAPER, botm.Pac.SCISSORS)

        if not poss:
            return

        for bot in self.bots:
            for pac_id in range(nb_pac_by_bot):
                if not poss:
                    break

                if bot.bot_id == 0:
                    pos = poss.pop()
                    if random.choice((False, True)):
                        pos = self.maze.symmetrical(pos)
                else:
                    pos = self.maze.symmetrical(self.pacs[0][pac_id].pos)

                pac = PacGame(bot.bot_id,
                              pac_id, pos, random.choice(type_ids))
                self.pacs[bot.bot_id].append(pac)
                self.maze.set_cell(pos, botm.Maze.EMPTY)

    def _update_game(self):
        self._update_game_ability_cooldown()
        self._update_game_speed_turns_left()
        self._update_game_abilities()
        self._update_game_move()
        self._update_game_kill()
        self._update_game_eat_pellet()

    def _update_game_abilities(self):
        pass

    def _update_game_ability_cooldown(self):
        for bot_id in range(2):
            for pac in self.pacs[bot_id]:
                if pac.ability_cooldown > 0:
                    pac.ability_cooldown -= 1

    def _update_game_eat_pellet(self):
        for bot_id in range(2):
            for pac in self.pacs[bot_id]:
                cell = self.maze.get_cell(pac.pos)
                if cell >= botm.Maze.SUPER_PELLET:
                    self.maze.set_cell(pac.pos, botm.Maze.EMPTY)
                    self.scores[bot_id] += botm.Maze.PELLET_VALUES[cell]

    def _update_game_move(self):  # pylint: disable=too-many-branches
        """
        Move pacs with dealing conflict in the same place,
        but not checking of crossing.
        """
        next_poss = dict()

        # Compute next positions
        for bot_id in range(2):
            for pac in self.pacs[bot_id]:
                if pac.destination == pac.pos:
                    pac.destination = None

                if pac.is_alive:
                    next_pos = (
                        pac.pos if pac.destination is None
                        else self.maze.compute_next_pos(pac.pos,
                                                        pac.destination))
                    pacs = next_poss.get(next_pos, set())
                    pacs.add(pac)
                    next_poss[next_pos] = pacs

        # Move, come back or battle
        while next_poss:  # pylint: disable=too-many-nested-blocks
            for next_pos, pacs in list(next_poss.items()):
                if len(pacs) == 1:
                    for pac in pacs:
                        pac.pos = next_pos

                    del next_poss[next_pos]
                else:
                    assert len(pacs) >= 2

                    # Check conflict
                    for pac_a in pacs:
                        for pac_b in pacs:
                            if pac_a == pac_b:
                                continue

                            looser = pac_a.battle_looser(pac_b)
                            if ((pac_a.bot_id == pac_b.bot_id) or
                                    (looser is None)):
                                # Same bot or tie
                                pac_a.destination = None
                                pac_b.destination = None
                            else:
                                # Different bot and there is a looser
                                looser.is_alive = False
                                looser.destination = None

            # Remove pacs moved or dead
            for next_pos, pacs in list(next_poss.items()):
                pacs = set(pac for pac in pacs if pac.destination is not None)

                if pacs:
                    next_poss[next_pos] = pacs
                else:
                    del next_poss[next_pos]

    def _update_game_kill(self):
        pass

    def _update_game_speed_turns_left(self):
        for bot_id in range(2):
            for pac in self.pacs[bot_id]:
                if pac.speed_turns_left > 0:
                    pac.speed_turns_left -= 1

    def _update_pacs_with_commands(self, commandss: Tuple[str, str]):  # noqa  # pylint: disable=too-many-branches
        def warning(command: str, commands: str, name: Optional[str] = None):
            name = ('' if name is None
                    else name + ' ')
            print("""Can't be run {}command "{}", in "{}"!"""
                  .format(name, command, commands), file=sys.stderr)

        def wrong(command: str, commands: str, name: Optional[str] = None):
            name = ('' if name is None
                    else name + ' ')
            print('Wrong {}command "{}", in "{}"!'
                  .format(name, command, commands), file=sys.stderr)

            sys.exit(1)

        for bot_id, commands in enumerate(commandss):
            for command in commands.split('|'):
                command = command.strip()
                pieces = command.split()
                if not pieces:
                    continue

                if pieces[0].strip() == 'SPEED':
                    try:
                        pac_id = int(pieces[1].strip())
                        if not 0 <= pac_id < self._nb_pac_by_bot:
                            raise ValueError
                    except (IndexError, ValueError):
                        wrong(command, commands, 'SPEED')
                    text = ' '.join(pieces[2:])
                    if not self.pacs[bot_id][pac_id].command_speed(text):
                        warning(command, commands, 'SPEED')
                elif pieces[0].strip() == 'SWITCH':
                    try:
                        pac_id = int(pieces[1].strip())
                        type_id = botm.Pac.TYPE_TO_IDS[pieces[2].strip()]
                        if not 0 <= pac_id < self._nb_pac_by_bot:
                            raise ValueError
                    except (IndexError, ValueError):
                        wrong(command, commands, 'SWITCH')
                    text = ' '.join(pieces[3:])
                    if not self.pacs[bot_id][pac_id].command_switch(type_id,
                                                                    text):
                        warning(command, commands, 'SPEED')
                elif pieces[0].strip() == 'MOVE':
                    try:
                        pac_id = int(pieces[1].strip())
                        pos_x = int(pieces[2].strip())
                        pos_y = int(pieces[3].strip())
                        if not 0 <= pac_id < self._nb_pac_by_bot:
                            raise ValueError
                        if not 0 <= pos_x < self.maze.width:
                            raise ValueError
                        if not 0 <= pos_y < self.maze.height:
                            raise ValueError
                    except (IndexError, ValueError):
                        wrong(command, commands, 'MOVE')
                    pos = botm.Pos(pos_x, pos_y)
                    text = ' '.join(pieces[4:])
                    if not self.pacs[bot_id][pac_id].command_move(pos, text):
                        warning(command, commands, 'SPEED')
                else:
                    wrong(command, commands)

    def _visible_poss(self, bot_id: int) -> FrozenSet[botm.Pos]:
        poss = set()  # type: Set[botm.Pos]
        for pac in self.pacs[bot_id]:
            poss.update(self.maze.visible_poss(pac.pos))

        return frozenset(poss)

    def _write_data(self):
        for bot in self.bots:
            visible_poss = self._visible_poss(bot.bot_id)

            # Write score
            bot.reader.add_line('{} {}'.format(*self.scores))

            # Write pacs (mines and opponents)
            seq = []
            for pac in self.pacs[bot.bot_id]:
                if pac.is_alive:
                    seq.append('{} 1 {} {} {} {}'
                               .format(pac.pac_id, pac.pos,
                                       botm.Pac.ID_TO_TYPES[pac.type_id],
                                       pac.speed_turns_left,
                                       pac.ability_cooldown))
            for pac in self.pacs[(bot.bot_id + 1) % 2]:
                if pac.is_alive and (pac.pos in visible_poss):
                    seq.append('{} 0 {} {} 0 0'
                               .format(pac.pac_id, pac.pos,
                                       botm.Pac.ID_TO_TYPES[pac.type_id]))

            seq.insert(0, str(len(seq)))
            bot.reader.add_lines(seq)

            # Write pellets (super pellets and visible normal pellets)
            seq = []
            for pos_y, row in enumerate(self.maze.rows):
                for pos_x, cell in enumerate(row):
                    pos = botm.Pos(pos_x, pos_y)
                    if ((cell >= botm.Maze.SUPER_PELLET) and
                            ((cell == botm.Maze.SUPER_PELLET) or
                             (pos in visible_poss))):
                        seq.append('{} {}'
                                   .format(pos, botm.Maze.PELLET_VALUES[cell]))
            seq.insert(0, str(len(seq)))
            bot.reader.add_lines(seq)

    def play(self, tour: int):
        assert 0 <= tour <= self.nb_tour

        if tour == self.tour:
            return

        if tour < len(HISTORY):  # get saved tour
            if self.display_function is not None:
                self._get_saved_tour_display(tour)
                self.display_function()  # pylint: disable=not-callable

            self._get_saved_tour(tour)
        else:                    # compute required tour(s)
            self.tour = len(HISTORY) - 1
            self._get_saved_tour(self.tour)
            while self.tour < tour:
                self.tour += 1
                self._write_data()
                commandss = (self.bots[0].play(1), self.bots[1].play(1))

                if self.display_function is not None:
                    HISTORY_DISPLAY.append(copy.deepcopy(self))
                    self.display_function()  # pylint: disable=not-callable

                self._update_pacs_with_commands(commandss)
                self._update_game()

                HISTORY.append(copy.deepcopy(self))

                # Check if finish
                has_pellet = any((self.maze.get_cell(botm.Pos(x, y)) >=
                                  botm.Maze.SUPER_PELLET)
                                 for y in range(self.maze.height)
                                 for x in range(self.maze.width))
                has_alive = (self.bots[0].living_my_pacs and
                             self.bots[1].living_my_pacs)
                if not has_pellet or not has_alive:
                    self.nb_tour = self.tour
                    tour = self.tour


class PacGame(botm.Pac):
    _ABILITY_COOLDOWN = 10
    _SPEED_TURNS_LEFT = 5

    def __init__(self, bot_id: int,
                 pac_id: int, pos: botm.Pos, type_id: int) -> None:
        assert 0 <= bot_id <= 1
        assert pac_id >= 0
        assert botm.Pac.ROCK <= type_id <= botm.Pac.SCISSORS

        super().__init__(bot_id,
                         pac_id, pos, type_id,
                         0, 0)

        self.is_alive = True
        self.destination = None  # type: Optional[botm.Pos]
        self.next_pos = None
        self.text = ''

    def __repr__(self):
        return str(self)

    def __str__(self):
        return ('{} -> ({}) -> ({}) "{}"'
                .format(super().__str__(),
                        self.next_pos, self.destination, self.text))

    def command_move(self, pos: botm.Pos, text: str) -> bool:
        self.destination = (None if self.pos == pos
                            else pos)
        self.text = text

        return True

    def command_speed(self, text: str) -> bool:
        if self.ability_cooldown != 0:
            return False

        self.speed_turns_left = PacGame._SPEED_TURNS_LEFT
        self.text = text

        self.ability_cooldown = PacGame._ABILITY_COOLDOWN
        # self.destination = pos = None

        return True

    def command_switch(self, type_id: int, text: str) -> bool:
        assert botm.Pac.ROCK <= type_id <= botm.Pac.SCISSORS

        if self.ability_cooldown != 0:
            return False

        self.type_id = type_id
        self.text = text

        self.ability_cooldown = PacGame._ABILITY_COOLDOWN
        # self.destination = pos = None

        return True


#
# Main
######
def main():
    random.seed(666)

    reader = botm.Reader()
    maze = botm.Maze(reader)

    game = Game(maze, 5, 2)
    game.play(200)


if __name__ == '__main__':
    main()
