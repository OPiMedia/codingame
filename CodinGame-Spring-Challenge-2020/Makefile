# Static checks --- May 18, 2020
.SUFFIXES:

SRC = $(sort $(wildcard *.py)) $(sort $(wildcard tests/*.py))


PYTHON      = python3  # https://www.python.org/
PYTHONFLAGS =

PYPY      = pypy3  # https://www.pypy.org/
PYPYFLAGS =


PYDEPS      = pydeps  # https://github.com/thebjorn/pydeps
PYDEPSFLAGS = --noshow

PYREVERSE      = pyreverse  # part of pylint, require https://www.graphviz.org/ for SVG format
PYREVERSEFLAGS = -A -my


MYPY      = mypy  # http://www.mypy-lang.org/
MYPYFLAGS = # --warn-unused-ignores

PYCODESTYLE      = pycodestyle  # (pep8) https://pypi.org/project/pycodestyle/
PYCODESTYLEFLAGS = --statistics  # --ignore=E501

PYDOCSTYLE      = pydocstyle  # http://www.pydocstyle.org/
PYDOCSTYLEFLAGS = --ignore D203,D212  # http://www.pydocstyle.org/en/latest/error_codes.html

PYFLAKES      = pyflakes  # https://pypi.org/project/pyflakes/
PYFLAKESFLAGS =

PYLINT      = pylint  # https://www.pylint.org/
PYLINTFLAGS = -j $(JOB) --disable=line-too-long,locally-disabled,missing-class-docstring,missing-function-docstring,protected-access

PYTYPE      = pytype  # https://google.github.io/pytype/
PYTYPEFLAGS = -k -j $(JOB)


PYTEST      = $(PYTHON) $(PYTHONFLAGS) -m pytest  # https://docs.pytest.org/
PYTESTFLAGS = -s -v

PYTESTPYPY      = $(PYPY) $(PYPYFLAGS) -m pytest
PYTESTPYPYFLAGS = -s -v


CD    = cd
ECHO  = echo
GREP  = grep
GZIP  = gzip
MAKE  = make
MKDIR = mkdir -p
MV    = mv
RM    = rm -f
SHELL = sh
TEE   = tee



JOB ?= 1  # change this by define new value when start: $ make JOB=3



###
# #
###
.PHONY: all diagrams logs pydeps pyreverse

all:	lintlog

diagrams:	pydeps pyreverse

logs:	lintlog testslog

pydeps:
	@$(ECHO)
	$(MKDIR) diagrams/pydeps
	$(PYDEPS) $(PYDEPSFLAGS) gametkm.py --cluster -o diagrams/pydeps/pydeps_all.svg

pyreverse:
	@$(ECHO)
	$(PYREVERSE) $(PYREVERSEFLAGS) -p gametkm *.py
	$(PYREVERSE) $(PYREVERSEFLAGS) -f ALL -p gametkm_all *.py
	-$(PYREVERSE) $(PYREVERSEFLAGS) -p gametkm -o svg *.py
	-$(PYREVERSE) $(PYREVERSEFLAGS) -f ALL -p gametkm_all -o svg *.py
	$(MKDIR) diagrams/pyreverse
	$(MV) classes_gametkm_all.* diagrams/pyreverse/
	$(MV) classes_gametkm.* diagrams/pyreverse/
	$(MV) packages_gametkm_all.* diagrams/pyreverse/
	$(RM) packages_gametkm.*



#################
# Static checks #
#################
.PHONY: lint lintlog mypy pycodestyle pydocstyle pyflakes pylint pytype pytypeTree pytypeUnresolved

lint:	pycodestyle pyflakes pylint pytypeTree pytypeUnresolved pytype mypy # pydocstyle

lintlog:
	$(ECHO) Lint | $(TEE) lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC) 2>&1 | $(GREP) -v 'imported but unused' | $(TEE) -a lint.log || exit 0 && exit 1
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYLINT) $(PYLINTFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pytype --unresolved ===== | $(TEE) -a lint.log
	-$(PYTYPE) $(PYTYPEFLAGS) --unresolved $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYTYPE) $(PYTYPEFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(MYPY) $(MYPYFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	# @$(ECHO) ===== pydocstyle `$(PYDOCSTYLE) $(PYDOCSTYLEFLAGS) --version` ===== | $(TEE) -a lint.log
	# -$(PYDOCSTYLE) $(PYDOCSTYLEFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log

mypy:
	@$(ECHO)
	-$(MYPY) $(MYPYFLAGS) $(SRC)

pycodestyle:
	@$(ECHO)
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC)

pydocstyle:
	@$(ECHO)
	-$(PYDOCSTYLE) $(PYDOCSTYLEFLAGS) $(SRC)

pyflakes:
	@$(ECHO)
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC) 2>&1 | $(GREP) -v 'imported but unused' | $(GREP) -E -v 'impleGUICS2Pygame/simpleguics2pygame/__init__.py:.+undefined name' || exit 0 && exit 1

pylint:
	@$(ECHO)
	-$(PYLINT) $(PYLINTFLAGS) -f colorized $(SRC)

pytype:
	@$(ECHO)
	-$(PYTYPE) $(PYTYPEFLAGS) $(SRC)

pytypeTree:
	@$(ECHO)
	-$(PYTYPE) --tree $(PYTYPEFLAGS) $(SRC)

pytypeUnresolved:
	@$(ECHO)
	-$(PYTYPE) --unresolved $(PYTYPEFLAGS) $(SRC)



#########
# Tests #
#########
.PHONY: pytests pytest pytestpypy testslog vv

pytests: pytest pytestpypy

pytest:
	-export PYTHONPATH=$(PWD):$(PYTHONPATH); $(CD) tests/; $(PYTEST) $(PYTESTFLAGS) test__*.py

pytestpypy:
	-export PYTHONPATH=$(PWD):$(PYTHONPATH); $(CD) tests/; $(PYTESTPYPY) $(PYTESTPYPYFLAGS) test__*.py

testslog:
	@$(ECHO) "Tests ("`date`") of gametkm" 2>&1 | $(TEE) tests/tests.log
	@$(ECHO) | $(TEE) -a tests/tests.log
	@$(ECHO) ===== pytest ===== | $(TEE) -a tests/tests.log
	-export PYTHONPATH=$(PWD):$(PYTHONPATH); $(CD) tests/; $(PYTEST) $(PYTESTFLAGS) test__*.py 2>&1 | $(TEE) -a tests.log
	@$(ECHO) | $(TEE) -a tests/tests.log
	@$(ECHO) ===== pytest with pypy ===== | $(TEE) -a tests/tests.log
	-export PYTHONPATH=$(PWD):$(PYTHONPATH); $(CD) tests/; $(PYTEST) $(PYTESTFLAGS) test__*.py 2>&1 | $(TEE) -a tests.log

vv:
	$(eval PYTESTFLAGS += -vv)
	$(eval PYTESTPYPYFLAGS += -vv)



#########
# Clean #
#########
.PHONY:	clean distclean overclean

clean:
	$(RM) -r __pycache__ tests/__pycache__
	$(RM) -r .mypy_cache
	$(RM) -r .pytype
	$(RM) -r .pytest_cache tests/.pytest_cache

distclean:	clean

overclean:	distclean
	$(RM) lint.log tests/tests.log
	$(RM) diagrams/pydeps/pydeps_*.svg
	$(RM) diagrams/pyreverse/classes_gametkm*.dot
	$(RM) diagrams/pyreverse/classes_gametkm*.svg
	$(RM) diagrams/pyreverse/packages_gametkm*.dot
	$(RM) diagrams/pyreverse/packages_gametkm*.svg
