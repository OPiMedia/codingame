# CodinGame Spring Challenge 2020

Bots in a Pac-Man maze.

- `botm.py`: my solution (1212th/4955, 578th/989 in silver league)
- `gamem.py`: my (partial) reimplementation of the problem engine
- `gametkm.py`: a Tk GUI for this engine
- [This Bitbucket repository](https://bitbucket.org/OPiMedia/codingame/src/master/CodinGame-Spring-Challenge-2020/)



## Links on CodinGame

- [CodinGame Spring Challenge 2020](https://www.codingame.com/contests/spring-challenge-2020/) with ranking
- [one match replay](https://www.codingame.com/replay/467268747)



![gametkm](https://bitbucket.org/OPiMedia/codingame/raw/master/CodinGame-Spring-Challenge-2020/_img/gametkm.png)
