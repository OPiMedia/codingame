#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
dump.

Copy to stderr data received from stdin,
for the engine of CodingGame Spring Challenge 2020.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: May 17, 2020
"""

import sys


#
# Functions
###########
def read_line() -> str:
    line = sys.stdin.readline().rstrip('\n')
    print(line, file=sys.stderr)
    sys.stderr.flush()

    return line


def read_lines(nb_line: int):
    for _ in range(nb_line):
        read_line()


#
# Main
######
def main():
    # Read and dump maze
    line = read_line()
    read_lines(int(line.split()[1]))

    while True:
        # Read and dump scores
        line = read_line()

        # Read and dump pacs
        line = read_line()
        read_lines(int(line))

        # Read and dump pellets
        line = read_line()
        read_lines(int(line))

        # Write (empty) command
        print()
        sys.stdout.flush()


if __name__ == '__main__':
    main()
