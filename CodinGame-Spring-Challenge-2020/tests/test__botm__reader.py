# -*- coding: utf-8 -*-

"""
test__botm__reader.py.

Test of class Reader.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 15, 2020
"""

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm


def test__add_line():
    reader = botm.Reader(('ab', '', 'cde gh', 'ijk'))

    assert tuple(reader.read_line() for _ in range(4)) == ('ab', '', 'cde gh', 'ijk')  # noqa

    reader.add_line(' 123')

    assert tuple(reader.read_line() for _ in range(1)) == (' 123', )

    reader.add_line('45 ')

    assert tuple(reader.read_line() for _ in range(1)) == ('45 ', )

    reader.add_line('first')
    reader.add_line('second')
    reader.add_line('third')

    assert tuple(reader.read_line() for _ in range(3)) == ('first', 'second', 'third')  # noqa

    assert reader._lines == []


def test__add_lines():
    reader = botm.Reader(('ab', '', 'cde gh', 'ijk'))

    assert tuple(reader.read_line() for _ in range(4)) == ('ab', '', 'cde gh', 'ijk')  # noqa

    reader.add_lines((' 123', '45 ', '  '))

    assert tuple(reader.read_line() for _ in range(3)) == (' 123', '45 ', '  ')

    reader.add_lines((' 123', '45 ', '  '))

    assert tuple(reader.read_line() for _ in range(1)) == (' 123', )

    reader.add_lines(('first', 'second', 'third'))

    assert tuple(reader.read_line() for _ in range(5)) == ('45 ', '  ', 'first', 'second', 'third')  # noqa

    assert reader._lines == []


def test__read_lines():
    reader = botm.Reader(('ab', '', 'cde gh', 'ijk'))

    assert tuple(reader.read_line() for _ in range(4)) == ('ab', '', 'cde gh', 'ijk')  # noqa
    assert reader._lines == []
