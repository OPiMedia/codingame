# -*- coding: utf-8 -*-

"""
test__gamem__pacgame.py.

Test specific methods of class PacMine.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 18, 2020
"""

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm
import gamem


def test__str():
    assert(str(gamem.PacGame(0, 0, botm.Pos(3, 2), botm.Pac.ROCK)) ==
           'PacGame 0 (3 2)->(3 2) ROCK speed 0 ability 0 -> (None) -> (None) ""')  # noqa
