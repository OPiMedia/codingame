# -*- coding: utf-8 -*-

"""
test__botm__pac.py.

Test of class Pac and subclasses PacMine and PacOpponent.

.. warning::
   Due to the unique cache in Pac (and subclass PacMine)
   following tests must be run separately (parallelism is prohibed).

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 18, 2020
"""

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm


MINE0_DATA = (0, botm.Pos(3, 2), botm.Pac.ROCK, 2, 3)
MINE1_DATA = (1, botm.Pos(1, 1), botm.Pac.PAPER, 0, 0)

OPPONENT0_DATA = (0, botm.Pos(3, 1), botm.Pac.PAPER, 3, 2)
OPPONENT1_DATA = (1, botm.Pos(1, 2), botm.Pac.SCISSORS, 0, 0)


MINE0_STR = '0 1 3 2 ROCK 2 3'
MINE1_STR = '1 1 1 1 PAPER 0 0'

OPPONENT0_STR = '0 0 3 1 PAPER 3 2'
OPPONENT1_STR = '1 0 1 2 SCISSORS 0 0'


MINE0_REPR = 'PacMine(0, Pos(3, 2), "ROCK", 2, 3)'
MINE1_REPR = 'PacMine(1, Pos(1, 1), "PAPER", 0, 0)'

OPPONENT0_REPR = 'PacOpponent(0, Pos(3, 1), "PAPER", 3, 2)'
OPPONENT1_REPR = 'PacOpponent(1, Pos(1, 2), "SCISSORS", 0, 0)'


def test__battle():
    assert botm.Pac.battle(botm.Pac.ROCK, botm.Pac.ROCK) is None
    assert botm.Pac.battle(botm.Pac.PAPER, botm.Pac.PAPER) is None
    assert botm.Pac.battle(botm.Pac.SCISSORS, botm.Pac.SCISSORS) is None

    assert botm.Pac.battle(botm.Pac.ROCK, botm.Pac.PAPER) is False
    assert botm.Pac.battle(botm.Pac.ROCK, botm.Pac.SCISSORS) is True

    assert botm.Pac.battle(botm.Pac.PAPER, botm.Pac.ROCK) is True
    assert botm.Pac.battle(botm.Pac.PAPER, botm.Pac.SCISSORS) is False

    assert botm.Pac.battle(botm.Pac.SCISSORS, botm.Pac.ROCK) is False
    assert botm.Pac.battle(botm.Pac.SCISSORS, botm.Pac.PAPER) is True


def test__battle_looser():
    rock = botm.PacMine(0, 0, botm.Pos(0, 0), botm.Pac.ROCK, 0, 0)
    paper = botm.PacMine(0, 0, botm.Pos(0, 0), botm.Pac.PAPER, 0, 0)
    scissors = botm.PacMine(0, 0, botm.Pos(0, 0), botm.Pac.SCISSORS, 0, 0)

    assert rock.battle_looser(rock) is None
    assert paper.battle_looser(paper) is None
    assert scissors.battle_looser(scissors) is None

    assert rock.battle_looser(paper) == rock
    assert rock.battle_looser(scissors) == scissors

    assert paper.battle_looser(rock) == rock
    assert paper.battle_looser(scissors) == paper

    assert scissors.battle_looser(rock) == scissors
    assert scissors.battle_looser(paper) == paper


def test__battle_winner():
    rock = botm.PacMine(0, 0, botm.Pos(0, 0), botm.Pac.ROCK, 0, 0)
    paper = botm.PacMine(0, 0, botm.Pos(0, 0), botm.Pac.PAPER, 0, 0)
    scissors = botm.PacMine(0, 0, botm.Pos(0, 0), botm.Pac.SCISSORS, 0, 0)

    assert rock.battle_winner(rock) is None
    assert paper.battle_winner(paper) is None
    assert scissors.battle_winner(scissors) is None

    assert rock.battle_winner(paper) == paper
    assert rock.battle_winner(scissors) == rock

    assert paper.battle_winner(rock) == paper
    assert paper.battle_winner(scissors) == scissors

    assert scissors.battle_winner(rock) == rock
    assert scissors.battle_winner(paper) == scissors


def test__constant():
    assert botm.Pac.ROCK < botm.Pac.PAPER < botm.Pac.SCISSORS

    for typ in botm.Pac.ID_TO_TYPES:
        assert botm.Pac.ID_TO_TYPES[botm.Pac.TYPE_TO_IDS[typ]] == typ

    for typ, type_id in botm.Pac.TYPE_TO_IDS.items():
        assert botm.Pac.ID_TO_TYPES[type_id] == typ


def test__pac_and_mypac():  # pylint: disable=too-many-statements
    # Add 2 my pac
    pac0 = botm.PacMine(0, 0, botm.Pos(3, 2), botm.Pac.ROCK, 2, 3)
    pac1 = botm.PacMine(0, 1, botm.Pos(1, 1), botm.Pac.PAPER, 0, 0)

    assert repr(pac0) == MINE0_REPR
    assert pac0.pac_id == 0
    assert pac0.pos == botm.Pos(3, 2)
    assert pac0.type_id == botm.Pac.ROCK
    assert pac0.speed_turns_left == 2
    assert pac0.ability_cooldown == 3
    assert pac0.previous_pos == botm.Pos(3, 2)
    assert pac0.destination is None
    assert pac0.blocked == 0
    assert pac0.to_speed is False
    assert pac0.to_switch is None

    assert repr(pac1) == MINE1_REPR
    assert pac1.pac_id == 1
    assert pac1.pos == botm.Pos(1, 1)
    assert pac1.type_id == botm.Pac.PAPER
    assert pac1.speed_turns_left == 0
    assert pac1.ability_cooldown == 0
    assert pac1.previous_pos == botm.Pos(1, 1)
    assert pac1.destination is None
    assert pac1.blocked == 0
    assert pac1.to_speed is False
    assert pac1.to_switch is None

    # Add 2 opponents
    opponent1 = botm.PacOpponent(0, 1, botm.Pos(1, 2), botm.Pac.SCISSORS, 0, 0)
    opponent0 = botm.PacOpponent(0, 0, botm.Pos(3, 1), botm.Pac.PAPER, 3, 2)

    assert repr(opponent0) == OPPONENT0_REPR
    assert opponent0.pac_id == 0
    assert opponent0.pos == botm.Pos(3, 1)
    assert opponent0.type_id == botm.Pac.PAPER
    assert opponent0.speed_turns_left == 3
    assert opponent0.ability_cooldown == 2
    assert opponent0.is_visible is True
    assert opponent0.previous_pos == botm.Pos(3, 1)

    assert repr(opponent1) == OPPONENT1_REPR
    assert opponent1.pac_id == 1
    assert opponent1.pos == botm.Pos(1, 2)
    assert opponent1.type_id == botm.Pac.SCISSORS
    assert opponent1.speed_turns_left == 0
    assert opponent1.ability_cooldown == 0
    assert opponent1.is_visible is True
    assert opponent1.previous_pos == botm.Pos(1, 2)


def test__repr():
    assert repr(botm.PacMine(0, *MINE0_DATA)) == MINE0_REPR
    assert repr(botm.PacMine(0, *MINE1_DATA)) == MINE1_REPR

    assert repr(botm.PacOpponent(1, *OPPONENT0_DATA)) == OPPONENT0_REPR
    assert repr(botm.PacOpponent(1, *OPPONENT1_DATA)) == OPPONENT1_REPR


def test__str():
    assert str(botm.PacOpponent(1, *OPPONENT0_DATA)) == 'PacOpponent 0 (3 1)->(3 1) PAPER speed 3 ability 2'  # noqa
    assert str(botm.PacOpponent(1, *OPPONENT1_DATA)) == 'PacOpponent 1 (1 2)->(1 2) SCISSORS speed 0 ability 0'  # noqa

    assert str(botm.PacMine(0, *MINE0_DATA)) == 'PacMine 0 (3 2)->(3 2) ROCK speed 2 ability 3 destination (None) blocked 0 to_speed False to_switch None'  # noqa
    assert str(botm.PacMine(0, *MINE1_DATA)) == 'PacMine 1 (1 1)->(1 1) PAPER speed 0 ability 0 destination (None) blocked 0 to_speed False to_switch None'  # noqa


def test__update():
    # Add opponent
    opponent = botm.PacOpponent(0, 42, botm.Pos(3, 1), botm.Pac.PAPER, 3, 2)

    assert str(opponent) == 'PacOpponent 42 (3 1)->(3 1) PAPER speed 3 ability 2'  # noqa
    assert opponent.is_visible is True

    opponent._update(botm.Pos(5, 0), botm.Pac.ROCK, 4, 7)

    assert str(opponent) == 'PacOpponent 42 (3 1)->(5 0) ROCK speed 4 ability 7'  # noqa
    assert opponent.is_visible is True

    # Add my pac
    pac = botm.PacMine(0, 36, botm.Pos(13, 21), botm.Pac.SCISSORS, 55, 66)

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch None'  # noqa

    pac._update(botm.Pos(5, 0), botm.Pac.ROCK, 4, 7)

    assert str(pac) == 'PacMine 36 (13 21)->(5 0) ROCK speed 4 ability 7 destination (None) blocked 0 to_speed False to_switch None'  # noqa
