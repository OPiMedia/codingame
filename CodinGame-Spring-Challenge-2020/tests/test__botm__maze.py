# -*- coding: utf-8 -*-

"""
test__botm__maze.py.

Test of class Maze.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 18, 2020
"""

import copy

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm


MAZE0_STR = """5 4
#####
# # #
  #  
#####"""  # noqa

MAZE1_STR = """33 12
#################################
#       #   #   #   #   #       #
# # ##### ### # # # ### ##### # #
# #           #   #           # #
# ### ##### # ##### # ##### ### #
#     #       #   #       #     #
##### # ### # # # # # ### # #####
#           #   #   #           #
# # ### ##### # # # ##### ### # #
# #   #       #   #       #   # #
##### # ##### ##### ##### # #####
#################################"""

MAZE2_STR = """33 12
#################################
#3      #   #   #   #   #      3#
#3# ##### ### # # # ### ##### #3#
43#  ......   #   #   ......  #34
5 ###.##### # ##### # #####.### 5
#.....#       #   #       #.....#
#####.# ### # # # # # ### #.#####
#        o  #   #   #  o        #
# # ### ##### # # # ##### ### # #
# #222#       #   #       #222# #
##### # ##### ##### ##### # #####
#################################"""

# Example with some cells not symmetrical
MAZE3_STR = """31 13
###############################
3333#.33#3#3#3###.#3#3#33.#3333
###3# ###3#3#3###.#3#3### #3###
#333#333#333#3333.#333#332#333#
#3### #3#3###3###.###3#3# ###3#
#o323.#...... ... ......#.333o#
#3#2#.###3###3###2###3###.#3#3#
#3# 2 #o3333# 222 #3333o#.3 #3#
#####.#3###3#3###.#3###3#.#####
#3#33.33#333#3333.#333#33.33#3#
#3#3#.#3#3#3#3###.#3#3#3#.#3#3#
#333#.#333#. 2 .  ..#333#.#333#
###############################"""


MAZE0 = None
MAZE1 = None
MAZE2 = None
MAZE3 = None


def setup_module():
    global MAZE0, MAZE1, MAZE2, MAZE3  # pylint: disable=global-statement

    MAZE0 = botm.Maze(reader=botm.Reader(MAZE0_STR.split('\n')))
    MAZE1 = botm.Maze(reader=botm.Reader(MAZE1_STR.split('\n')))
    MAZE2 = botm.Maze(reader=botm.Reader(MAZE2_STR.split('\n')))
    MAZE3 = botm.Maze(reader=botm.Reader(MAZE3_STR.split('\n')))


def test__clear_and_set_cell():
    maze1 = copy.deepcopy(MAZE1)
    maze1.full_normal_pellets()

    for pos_x in range(1, 8):
        pos = botm.Pos(pos_x, 1)

        assert maze1.get_cell(pos) == botm.Maze.NORMAL_PELLET
        assert maze1.is_wall(pos) is False
        assert maze1.is_super_pellet(pos) is False
        assert maze1.is_normal_pellet(pos) is True
        assert maze1.is_maybe_pellet(pos) is True

        maze1.set_cell(pos, botm.Maze.SUPER_PELLET)

        assert maze1.get_cell(pos) == botm.Maze.SUPER_PELLET
        assert maze1.is_wall(pos) is False
        assert maze1.is_super_pellet(pos) is True
        assert maze1.is_normal_pellet(pos) is False
        assert maze1.is_maybe_pellet(pos) is True

        assert MAZE1.is_certainly_empty(pos) is True

        maze1.clear_cell(pos)

        assert maze1.get_cell(pos) == botm.Maze.EMPTY
        assert maze1.is_wall(pos) is False
        assert maze1.is_super_pellet(pos) is False
        assert maze1.is_normal_pellet(pos) is False
        assert maze1.is_maybe_pellet(pos) is False

        assert MAZE1.is_certainly_empty(pos)


def test__close_dead_end():
    # maze0
    maze = botm.Maze(reader=botm.Reader(MAZE0_STR.split('\n')))
    maze.close_dead_end(botm.Pos(3, 2))

    assert str(maze) == """#####
# ###
  #  
#####"""  # noqa

    maze.close_dead_end(botm.Pos(1, 2))

    assert str(maze) == """#####
#####
# ###
#####"""

    # maze0
    maze = botm.Maze(reader=botm.Reader(MAZE0_STR.split('\n')))
    keep_poss = frozenset((botm.Pos(1, 2), botm.Pos(4, 2)))
    maze.close_dead_end(botm.Pos(3, 2), keep_poss=keep_poss)

    assert str(maze) == """#####
# ###
  #  
#####"""  # noqa

    maze.close_dead_end(botm.Pos(1, 2), keep_poss=keep_poss)

    assert str(maze) == """#####
#####
  ## 
#####"""  # noqa

    # maze1
    maze = botm.Maze(reader=botm.Reader(MAZE1_STR.split('\n')))
    keep_poss = frozenset((botm.Pos(28, 1), botm.Pos(4, 2)))
    maze.close_dead_end(botm.Pos(28, 1), keep_poss=keep_poss)

    assert str(maze)[:101] == """#################################
#       #   #   #   #   ####    #
# # ##### ### # # # ### ##### # #"""

    # maze1
    maze = botm.Maze(reader=botm.Reader(MAZE1_STR.split('\n')))
    keep_poss = frozenset((botm.Pos(28, 1), botm.Pos(4, 2)))
    maze.close_dead_end(botm.Pos(31, 1), keep_poss=keep_poss)

    assert str(maze)[:101] == """#################################
#       #   #   #   #   ####    #
# # ##### ### # # # ### ##### # #"""

    # maze1
    maze = botm.Maze(reader=botm.Reader(MAZE1_STR.split('\n')))
    maze.close_dead_end(botm.Pos(31, 1))

    assert str(maze)[:101] == """#################################
#       #   #   #   #   #####   #
# # ##### ### # # # ### ##### # #"""


def test__compute_next_pos():  # pylint: disable=too-many-statements
    # maze0
    assert MAZE0.compute_next_pos(botm.Pos(1, 1), botm.Pos(0, 2)) == botm.Pos(1, 2)  # noqa
    assert MAZE0.compute_next_pos(botm.Pos(1, 1), botm.Pos(3, 1)) == botm.Pos(1, 2)  # noqa

    # maze1
    next_pos = MAZE1.compute_next_pos(botm.Pos(15, 3), botm.Pos(17, 5))
    assert next_pos in (botm.Pos(15, 2), botm.Pos(16, 3))

    for _ in range(100):
        assert MAZE1.compute_next_pos(botm.Pos(15, 3), botm.Pos(17, 5)) == next_pos  # noqa

    assert MAZE1.compute_next_pos(botm.Pos(15, 3), botm.Pos(16, 5)) == botm.Pos(15, 2)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 3), botm.Pos(15, 5)) == botm.Pos(15, 2)  # noqa

    assert MAZE1.compute_next_pos(botm.Pos(15, 2), botm.Pos(17, 5)) == botm.Pos(15, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 1), botm.Pos(17, 5)) == botm.Pos(14, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(14, 1), botm.Pos(17, 5)) == botm.Pos(13, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 1), botm.Pos(17, 5)) == botm.Pos(13, 2)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 2), botm.Pos(17, 5)) == botm.Pos(13, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 3), botm.Pos(17, 5)) == botm.Pos(13, 4)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 4), botm.Pos(17, 5)) == botm.Pos(13, 5)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 5), botm.Pos(17, 5)) == botm.Pos(13, 6)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 6), botm.Pos(17, 5)) == botm.Pos(13, 7)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 7), botm.Pos(17, 5)) == botm.Pos(14, 7)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(14, 7), botm.Pos(17, 5)) == botm.Pos(15, 7)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 7), botm.Pos(17, 5)) == botm.Pos(15, 6)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 6), botm.Pos(17, 5)) == botm.Pos(15, 5)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 5), botm.Pos(17, 5)) == botm.Pos(16, 5)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(16, 5), botm.Pos(17, 5)) == botm.Pos(17, 5)  # noqa

    assert MAZE1.compute_next_pos(botm.Pos(29, 3), botm.Pos(11, 3)) == botm.Pos(28, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(28, 3), botm.Pos(11, 3)) == botm.Pos(27, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(27, 3), botm.Pos(11, 3)) == botm.Pos(26, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(26, 3), botm.Pos(11, 3)) == botm.Pos(25, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(25, 3), botm.Pos(11, 3)) == botm.Pos(24, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(24, 3), botm.Pos(11, 3)) == botm.Pos(23, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(23, 3), botm.Pos(11, 3)) == botm.Pos(22, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(22, 3), botm.Pos(11, 3)) == botm.Pos(21, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(21, 3), botm.Pos(11, 3)) == botm.Pos(20, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(20, 3), botm.Pos(11, 3)) == botm.Pos(19, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(19, 3), botm.Pos(11, 3)) == botm.Pos(19, 2)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(19, 2), botm.Pos(11, 3)) == botm.Pos(19, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(19, 1), botm.Pos(11, 3)) == botm.Pos(18, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(18, 1), botm.Pos(11, 3)) == botm.Pos(17, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(17, 1), botm.Pos(11, 3)) == botm.Pos(17, 2)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(17, 2), botm.Pos(11, 3)) == botm.Pos(17, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(17, 3), botm.Pos(11, 3)) == botm.Pos(16, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(16, 3), botm.Pos(11, 3)) == botm.Pos(15, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 3), botm.Pos(11, 3)) == botm.Pos(15, 2)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 2), botm.Pos(11, 3)) == botm.Pos(15, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(15, 1), botm.Pos(11, 3)) == botm.Pos(14, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(14, 1), botm.Pos(11, 3)) == botm.Pos(13, 1)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 1), botm.Pos(11, 3)) == botm.Pos(13, 2)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 2), botm.Pos(11, 3)) == botm.Pos(13, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(13, 3), botm.Pos(11, 3)) == botm.Pos(12, 3)  # noqa
    assert MAZE1.compute_next_pos(botm.Pos(12, 3), botm.Pos(11, 3)) == botm.Pos(11, 3)  # noqa

    # maze2
    assert MAZE2.compute_next_pos(botm.Pos(32, 3), botm.Pos(3, 2)) == botm.Pos(0, 3)  # noqa
    assert MAZE2.compute_next_pos(botm.Pos(0, 3), botm.Pos(3, 2)) == botm.Pos(1, 3)  # noqa
    assert MAZE2.compute_next_pos(botm.Pos(1, 3), botm.Pos(3, 2)) == botm.Pos(1, 2)  # noqa
    assert MAZE2.compute_next_pos(botm.Pos(1, 2), botm.Pos(3, 2)) == botm.Pos(1, 1)  # noqa
    assert MAZE2.compute_next_pos(botm.Pos(1, 1), botm.Pos(3, 2)) == botm.Pos(2, 1)  # noqa
    assert MAZE2.compute_next_pos(botm.Pos(2, 1), botm.Pos(3, 2)) == botm.Pos(3, 1)  # noqa
    assert MAZE2.compute_next_pos(botm.Pos(3, 1), botm.Pos(3, 2)) == botm.Pos(3, 2)  # noqa


def test__compute_next_poss():
    # maze0
    assert MAZE0.compute_next_poss(botm.Pos(1, 1), botm.Pos(0, 2)) == frozenset((botm.Pos(1, 2), ))  # noqa
    assert MAZE0.compute_next_poss(botm.Pos(1, 1), botm.Pos(3, 1)) == frozenset((botm.Pos(1, 2), ))  # noqa

    # maze1
    assert MAZE1.compute_next_poss(botm.Pos(15, 3), botm.Pos(17, 5)) == frozenset((botm.Pos(15, 2), botm.Pos(16, 3)))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(15, 3), botm.Pos(16, 5)) == frozenset((botm.Pos(15, 2), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(15, 3), botm.Pos(15, 5)) == frozenset((botm.Pos(15, 2), ))  # noqa

    assert MAZE1.compute_next_poss(botm.Pos(15, 2), botm.Pos(17, 5)) == frozenset((botm.Pos(15, 1), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(15, 1), botm.Pos(17, 5)) == frozenset((botm.Pos(14, 1), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(14, 1), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 1), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 1), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 2), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 2), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 3), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 3), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 4), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 4), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 5), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 5), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 6), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 6), botm.Pos(17, 5)) == frozenset((botm.Pos(13, 7), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(13, 7), botm.Pos(17, 5)) == frozenset((botm.Pos(14, 7), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(14, 7), botm.Pos(17, 5)) == frozenset((botm.Pos(15, 7), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(15, 7), botm.Pos(17, 5)) == frozenset((botm.Pos(15, 6), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(15, 6), botm.Pos(17, 5)) == frozenset((botm.Pos(15, 5), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(15, 5), botm.Pos(17, 5)) == frozenset((botm.Pos(16, 5), ))  # noqa
    assert MAZE1.compute_next_poss(botm.Pos(16, 5), botm.Pos(17, 5)) == frozenset((botm.Pos(17, 5), ))  # noqa


def test__compute_path_poss():
    # maze0
    assert (MAZE0.compute_path_poss(botm.Pos(1, 1), botm.Pos(0, 2)) ==
            (botm.Pos(1, 1),
             botm.Pos(1, 2), botm.Pos(0, 2)))
    assert (MAZE0.compute_path_poss(botm.Pos(1, 1), botm.Pos(3, 1)) ==
            (botm.Pos(1, 1),
             botm.Pos(1, 2), botm.Pos(0, 2),
             botm.Pos(4, 2), botm.Pos(3, 2),
             botm.Pos(3, 1)))

    # maze1
    path_poss = MAZE1.compute_path_poss(botm.Pos(15, 3), botm.Pos(17, 5))
    assert (path_poss in
            ((botm.Pos(15, 3),
              botm.Pos(15, 2),
              botm.Pos(15, 1), botm.Pos(14, 1), botm.Pos(13, 1),
              botm.Pos(13, 2),
              botm.Pos(13, 3),
              botm.Pos(13, 4),
              botm.Pos(13, 5),
              botm.Pos(13, 6),
              botm.Pos(13, 7), botm.Pos(14, 7), botm.Pos(15, 7),
              botm.Pos(15, 6),
              botm.Pos(15, 5), botm.Pos(16, 5), botm.Pos(17, 5)),
             (botm.Pos(15, 3), botm.Pos(16, 3), botm.Pos(17, 3),
              botm.Pos(17, 2),
              botm.Pos(17, 1), botm.Pos(18, 1), botm.Pos(19, 1),
              botm.Pos(19, 2),
              botm.Pos(19, 3),
              botm.Pos(19, 4),
              botm.Pos(19, 5),
              botm.Pos(19, 6),
              botm.Pos(19, 7), botm.Pos(18, 7), botm.Pos(17, 7),
              botm.Pos(17, 6),
              botm.Pos(17, 5))))

    for _ in range(100):
        assert MAZE1.compute_path_poss(botm.Pos(15, 3), botm.Pos(17, 5)) == path_poss  # noqa


def test__compute_paths_poss():
    # maze0
    assert (MAZE0.compute_paths_poss(botm.Pos(1, 1), botm.Pos(0, 2)) ==
            frozenset((botm.Pos(1, 1),
                       botm.Pos(1, 2), botm.Pos(0, 2))))
    assert (MAZE0.compute_paths_poss(botm.Pos(1, 1), botm.Pos(3, 1)) ==
            frozenset((botm.Pos(1, 1),
                       botm.Pos(1, 2), botm.Pos(0, 2),
                       botm.Pos(4, 2), botm.Pos(3, 2),
                       botm.Pos(3, 1))))

    # maze1
    assert (MAZE1.compute_paths_poss(botm.Pos(15, 3), botm.Pos(17, 5)) ==
            frozenset((botm.Pos(15, 3),
                       botm.Pos(15, 2),
                       botm.Pos(15, 1), botm.Pos(14, 1), botm.Pos(13, 1),
                       botm.Pos(13, 2),
                       botm.Pos(13, 3),
                       botm.Pos(13, 4),
                       botm.Pos(13, 5),
                       botm.Pos(13, 6),
                       botm.Pos(13, 7), botm.Pos(14, 7), botm.Pos(15, 7),
                       botm.Pos(15, 6),
                       botm.Pos(15, 5), botm.Pos(16, 5), botm.Pos(17, 5),
                       #
                       botm.Pos(16, 3), botm.Pos(17, 3),
                       botm.Pos(17, 2),
                       botm.Pos(17, 1), botm.Pos(18, 1), botm.Pos(19, 1),
                       botm.Pos(19, 2),
                       botm.Pos(19, 3),
                       botm.Pos(19, 4),
                       botm.Pos(19, 5),
                       botm.Pos(19, 6),
                       botm.Pos(19, 7), botm.Pos(18, 7), botm.Pos(17, 7),
                       botm.Pos(17, 6))))


def test__constants():
    assert (botm.Maze.WALL < botm.Maze.EMPTY < botm.Maze.SUPER_PELLET <
            botm.Maze.NORMAL_PELLET < botm.Maze.MAYBE_PELLET)

    for cell in range(-2, 2):
        assert botm.Maze._SYMBOL_TO_CELLS[botm.Maze._CELL_TO_SYMBOLS[cell]] == cell  # noqa

    for symbol in '# o.':
        assert botm.Maze._CELL_TO_SYMBOLS[botm.Maze._SYMBOL_TO_CELLS[symbol]] == symbol  # noqa


def test__distance():  # pylint: disable=too-many-statements
    def test(maze):
        for pos_xa in range(maze.width):
            for pos_ya in range(maze.height):
                pos_a = botm.Pos(pos_xa, pos_ya)
                if maze.is_wall(pos_a):
                    continue

                for pos_xb in range(maze.width):
                    for pos_yb in range(maze.height):
                        pos_b = botm.Pos(pos_xb, pos_yb)
                        if maze.is_wall(pos_b):
                            continue

                        if pos_a == pos_b:
                            assert maze.distance(pos_a, pos_b) == 0
                        else:
                            assert maze.distance(pos_a, pos_b) > 0
                            assert maze.distance(pos_a, pos_b) == maze.distance(pos_b, pos_a)  # noqa

    # maze0
    assert MAZE0.distance(botm.Pos(1, 1), botm.Pos(0, 2)) == 2
    assert MAZE0.distance(botm.Pos(1, 1), botm.Pos(3, 1)) == 5

    test(MAZE0)

    # maze1
    assert MAZE1.distance(botm.Pos(15, 3), botm.Pos(17, 5)) == 16
    assert MAZE1.distance(botm.Pos(15, 3), botm.Pos(16, 5)) == 15
    assert MAZE1.distance(botm.Pos(15, 3), botm.Pos(15, 5)) == 14

    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(11, 3)) == 26
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(11, 4)) == 27
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(11, 5)) == 28
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(10, 5)) == 29
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(9, 5)) == 30
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(8, 5)) == 31
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(7, 5)) == 32
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(7, 6)) == 33
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(7, 7)) == 34
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(6, 7)) == 35
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(5, 7)) == 36

    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(10, 3)) == 27
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(9, 3)) == 28
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(8, 3)) == 29
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(7, 3)) == 30
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(6, 3)) == 31
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(5, 3)) == 32
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(5, 4)) == 33
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(5, 5)) == 34
    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(5, 6)) == 35

    assert MAZE1.distance(botm.Pos(29, 3), botm.Pos(3, 5)) == 36
    assert MAZE1.distance(botm.Pos(29, 2), botm.Pos(3, 5)) == 37

    test(MAZE1)

    # maze2
    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(32, 3)) == 0

    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(0, 3)) == 1
    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(31, 3)) == 1
    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(32, 4)) == 1

    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(31, 2)) == 2
    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(1, 3)) == 2
    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(0, 4)) == 2
    assert MAZE2.distance(botm.Pos(32, 3), botm.Pos(31, 4)) == 2

    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(31, 2)) == 1
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(32, 3)) == 1
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(31, 4)) == 1

    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(31, 1)) == 2
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(0, 3)) == 2
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(32, 4)) == 2
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(31, 5)) == 2

    assert MAZE2.distance(botm.Pos(1, 3), botm.Pos(31, 3)) == 3

    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(30, 1)) == 3
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(1, 3)) == 3
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(0, 4)) == 3
    assert MAZE2.distance(botm.Pos(31, 3), botm.Pos(30, 5)) == 3

    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(32, 3)) == 7
    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(0, 3)) == 8
    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(3, 1)) == 13
    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(3, 3)) == 15
    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(5, 3)) == 17

    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(3, 5)) == 13
    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(5, 5)) == 15
    assert MAZE2.distance(botm.Pos(29, 3), botm.Pos(5, 4)) == 16

    test(MAZE2)

    # maze3
    test(MAZE3)


def test__full_normal_pellets():
    # maze0
    maze0 = copy.deepcopy(MAZE0)
    maze0.full_normal_pellets()

    assert str(maze0) == MAZE0_STR.replace('5 4\n', '').replace(' ', '.')

    # maze1
    maze1 = copy.deepcopy(MAZE1)
    maze1.full_normal_pellets()

    assert str(maze1) == MAZE1_STR.replace('33 12\n', '').replace(' ', '.')

    # maze2
    maze2 = copy.deepcopy(MAZE2)
    maze2.full_normal_pellets()

    assert str(maze2) == MAZE2_STR.replace('33 12\n', '').replace(' ', '.')

    # maze3
    maze3 = copy.deepcopy(MAZE3)
    maze3.full_normal_pellets()

    assert str(maze3) == MAZE3_STR.replace('31 13\n', '').replace(' ', '.')


def test__get_cell():
    # maze1
    for pos_x in range(33):
        assert MAZE1.get_cell(botm.Pos(pos_x, 0)) == botm.Maze.WALL
        assert MAZE1.get_cell(botm.Pos(pos_x, 11)) == botm.Maze.WALL

    for pos_x in range(33):
        for pos_y in range(12):
            assert MAZE1.get_cell(botm.Pos(pos_x, pos_y)) in (botm.Maze.WALL,
                                                              botm.Maze.EMPTY)

    # maze2
    assert MAZE2.get_cell(botm.Pos(30, 2)) == botm.Maze.WALL
    assert MAZE2.get_cell(botm.Pos(16, 3)) == botm.Maze.EMPTY
    assert MAZE2.get_cell(botm.Pos(9, 7)) == botm.Maze.SUPER_PELLET
    assert MAZE2.get_cell(botm.Pos(9, 3)) == botm.Maze.NORMAL_PELLET
    assert MAZE2.get_cell(botm.Pos(29, 9)) == 2
    assert MAZE2.get_cell(botm.Pos(31, 1)) == 3

    for pos_x in range(33):
        for pos_y in range(12):
            assert MAZE2.get_cell(botm.Pos(pos_x, pos_y)) in (botm.Maze.WALL,
                                                              botm.Maze.EMPTY,
                                                              botm.Maze.SUPER_PELLET,  # noqa
                                                              botm.Maze.NORMAL_PELLET,  # noqa
                                                              2, 3, 4, 5)


def test__get_maybe_pellet():
    # maze1
    assert MAZE1.get_maybe_pellet(botm.Pos(7, 3)) is None

    # maze2
    assert MAZE2.get_maybe_pellet(botm.Pos(7, 3)) == botm.Maze.NORMAL_PELLET
    assert MAZE2.get_maybe_pellet(botm.Pos(30, 2)) is None
    assert MAZE2.get_maybe_pellet(botm.Pos(16, 3)) is None
    assert MAZE2.get_maybe_pellet(botm.Pos(9, 7)) == botm.Maze.SUPER_PELLET
    assert MAZE2.get_maybe_pellet(botm.Pos(9, 3)) == botm.Maze.NORMAL_PELLET
    assert MAZE2.get_maybe_pellet(botm.Pos(29, 9)) == 2
    assert MAZE2.get_maybe_pellet(botm.Pos(31, 1)) == 3


def test__increase_maybe_pellets():
    maze1 = copy.deepcopy(MAZE1)
    maze1.full_normal_pellets()

    for i in range(2, 15):
        maze1.increase_maybe_pellets()

        assert str(maze1) == MAZE1_STR.replace('33 12\n', '').replace(' ', str(i % 10))  # noqa

    maze1 = copy.deepcopy(MAZE1)
    maze1.full_normal_pellets()
    maze1.clear_cell(botm.Pos(7, 3))
    maze1.set_cell(botm.Pos(5, 7), botm.Maze.SUPER_PELLET)
    maze1.set_cell(botm.Pos(28, 5), 36)

    data1 = str(maze1)
    maze1.increase_maybe_pellets()

    assert str(maze1) == data1.replace('6', '7').replace('.', '2')

    assert maze1.get_cell(botm.Pos(7, 3)) == botm.Maze.EMPTY
    assert maze1.get_cell(botm.Pos(5, 7)) == botm.Maze.SUPER_PELLET
    assert maze1.get_cell(botm.Pos(6, 7)) == 2
    assert maze1.get_cell(botm.Pos(28, 5)) == 37

    assert maze1.is_certainly_empty(botm.Pos(7, 3))
    assert maze1.is_super_pellet(botm.Pos(5, 7))
    assert maze1.is_maybe_pellet(botm.Pos(28, 5))

    assert maze1.get_maybe_pellet(botm.Pos(0, 0)) is None
    assert maze1.get_maybe_pellet(botm.Pos(7, 3)) is None
    assert maze1.get_maybe_pellet(botm.Pos(5, 7)) == botm.Maze.SUPER_PELLET
    assert maze1.get_maybe_pellet(botm.Pos(6, 7)) == 2
    assert maze1.get_maybe_pellet(botm.Pos(28, 5)) == 37

    for pos_x in range(33):
        for pos_y in range(12):
            assert not maze1.is_normal_pellet(botm.Pos(pos_x, pos_y))


def test__is_cell():
    # maze1
    assert MAZE1.is_wall(botm.Pos(0, 0))

    for pos_x in range(6, 11):
        assert MAZE1.is_wall(botm.Pos(pos_x, 4))

    for pos_x in range(3, 14):
        assert MAZE1.is_certainly_empty(botm.Pos(pos_x, 3))

    # maze2
    assert MAZE2.is_wall(botm.Pos(30, 2))
    assert MAZE2.is_certainly_empty(botm.Pos(16, 3))
    assert MAZE2.is_super_pellet(botm.Pos(9, 7))
    assert MAZE2.is_normal_pellet(botm.Pos(9, 3))
    assert MAZE2.is_maybe_pellet(botm.Pos(29, 9))
    assert MAZE2.is_maybe_pellet(botm.Pos(31, 1))


def test__paths_rows():  # pylint: disable=too-many-statements
    def test(maze):
        nb_empty = 0
        for pos_x in range(maze.width):
            for pos_y in range(maze.height):
                pos = botm.Pos(pos_x, pos_y)
                if not maze.is_wall(pos):
                    nb_empty += 1

        empty_set = maze.paths(botm.Pos(0, 0))[-1]
        for pos_x in range(maze.width):
            for pos_y in range(maze.height):
                pos = botm.Pos(pos_x, pos_y)
                path = maze.paths(pos)

                assert path[-1] == frozenset()
                assert id(path[-1]) == id(empty_set)

                for moves in path[:-1]:
                    assert isinstance(moves, frozenset)
                    assert moves

                if maze.is_wall(pos):
                    assert len(path) == 1
                else:
                    assert len(path) > 1

                    symmetric_path = maze.paths(pos.symmetrical(maze.width))

                    assert len(path) == len(symmetric_path)

                    for i, moves in enumerate(path):
                        assert len(moves) == len(symmetric_path[i])

                    # All moves are disjoint
                    all_moves = set((pos, ))
                    all_moves.update(*path)
                    nb_total = 1 + sum(len(moves) for moves in path)

                    assert len(all_moves) == nb_total

                    # All (empty) cells are reached
                    assert nb_total == nb_empty

    # maze0
    assert len(MAZE0._paths_rows) == 4
    assert len(MAZE0._paths_rows[0]) == 5

    assert MAZE0.paths(botm.Pos(0, 0)) == (frozenset(), )
    assert MAZE0.paths(botm.Pos(1, 1)) == (frozenset((botm.Pos(1, 2), )),
                                           frozenset((botm.Pos(0, 2), )),
                                           frozenset((botm.Pos(4, 2), )),
                                           frozenset((botm.Pos(3, 2), )),
                                           frozenset((botm.Pos(3, 1), )),
                                           frozenset())
    assert MAZE0.paths(botm.Pos(1, 2)) == (frozenset((botm.Pos(1, 1), botm.Pos(0, 2))),  # noqa
                                           frozenset((botm.Pos(4, 2), )),
                                           frozenset((botm.Pos(3, 2), )),
                                           frozenset((botm.Pos(3, 1), )),
                                           frozenset())
    assert MAZE0.paths(botm.Pos(0, 2)) == (frozenset((botm.Pos(1, 2), botm.Pos(4, 2))),  # noqa
                                           frozenset((botm.Pos(1, 1), botm.Pos(3, 2))),  # noqa
                                           frozenset((botm.Pos(3, 1), )),
                                           frozenset())
    assert MAZE0.paths(botm.Pos(4, 2)) == (frozenset((botm.Pos(0, 2), botm.Pos(3, 2))),  # noqa
                                           frozenset((botm.Pos(1, 2), botm.Pos(3, 1))),  # noqa
                                           frozenset((botm.Pos(1, 1), )),
                                           frozenset())
    assert MAZE0.paths(botm.Pos(3, 2)) == (frozenset((botm.Pos(3, 1), botm.Pos(4, 2))),  # noqa
                                           frozenset((botm.Pos(0, 2), )),
                                           frozenset((botm.Pos(1, 2), )),
                                           frozenset((botm.Pos(1, 1), )),
                                           frozenset())
    assert MAZE0.paths(botm.Pos(3, 1)) == (frozenset((botm.Pos(3, 2), )),
                                           frozenset((botm.Pos(4, 2), )),
                                           frozenset((botm.Pos(0, 2), )),
                                           frozenset((botm.Pos(1, 2), )),
                                           frozenset((botm.Pos(1, 1), )),
                                           frozenset())

    test(MAZE0)

    # maze1
    path = MAZE1.paths(botm.Pos(31, 3))

    assert path[0] == frozenset((botm.Pos(31, 2), botm.Pos(31, 4)))
    assert path[1] == frozenset((botm.Pos(31, 1), botm.Pos(31, 5)))
    assert path[2] == frozenset((botm.Pos(30, 1), botm.Pos(30, 5)))
    assert path[3] == frozenset((botm.Pos(29, 1), botm.Pos(29, 5)))
    assert path[4] == frozenset((botm.Pos(28, 1), botm.Pos(29, 2), botm.Pos(28, 5)))  # noqa
    assert path[5] == frozenset((botm.Pos(27, 1), botm.Pos(29, 3), botm.Pos(27, 5)))  # noqa

    path = MAZE1.paths(botm.Pos(13, 8))

    assert path[0] == frozenset((botm.Pos(13, 7), botm.Pos(13, 9)))
    assert path[1] == frozenset((botm.Pos(13, 6),
                                 botm.Pos(14, 7),
                                 botm.Pos(12, 9),
                                 botm.Pos(13, 10)))
    assert path[2] == frozenset((botm.Pos(13, 5),
                                 botm.Pos(15, 7),
                                 botm.Pos(11, 9)))
    assert path[3] == frozenset((botm.Pos(13, 4),
                                 botm.Pos(12, 5),
                                 botm.Pos(15, 6),
                                 botm.Pos(15, 8),
                                 botm.Pos(10, 9)))
    assert path[4] == frozenset((botm.Pos(13, 3),
                                 botm.Pos(11, 5), botm.Pos(15, 5),
                                 botm.Pos(15, 9),
                                 botm.Pos(9, 9)))
    assert path[5] == frozenset((botm.Pos(13, 2),
                                 botm.Pos(12, 3),
                                 botm.Pos(11, 4),
                                 botm.Pos(10, 5), botm.Pos(16, 5),
                                 botm.Pos(11, 6),
                                 botm.Pos(16, 9),
                                 botm.Pos(8, 9)))

    test(MAZE1)

    # maze2
    path = MAZE2.paths(botm.Pos(32, 3))

    assert path[0] == frozenset((botm.Pos(0, 3), botm.Pos(31, 3), botm.Pos(32, 4)))  # noqa
    assert path[1] == frozenset((botm.Pos(31, 2),
                                 botm.Pos(1, 3),
                                 botm.Pos(0, 4), botm.Pos(31, 4)))
    assert path[2] == frozenset((botm.Pos(31, 1),
                                 botm.Pos(1, 2),
                                 botm.Pos(1, 4),
                                 botm.Pos(31, 5)))

    path = MAZE2.paths(botm.Pos(31, 3))

    assert path[0] == frozenset((botm.Pos(31, 2), botm.Pos(32, 3), botm.Pos(31, 4)))  # noqa
    assert path[1] == frozenset((botm.Pos(31, 1),
                                 botm.Pos(0, 3),
                                 botm.Pos(32, 4),
                                 botm.Pos(31, 5)))
    assert path[2] == frozenset((botm.Pos(30, 1),
                                 botm.Pos(1, 3),
                                 botm.Pos(0, 4),
                                 botm.Pos(30, 5)))

    path = MAZE1.paths(botm.Pos(13, 8))

    assert path[0] == frozenset((botm.Pos(13, 7), botm.Pos(13, 9)))
    assert path[1] == frozenset((botm.Pos(13, 6),
                                 botm.Pos(14, 7),
                                 botm.Pos(12, 9),
                                 botm.Pos(13, 10)))
    assert path[2] == frozenset((botm.Pos(13, 5),
                                 botm.Pos(15, 7),
                                 botm.Pos(11, 9)))
    assert path[3] == frozenset((botm.Pos(13, 4),
                                 botm.Pos(12, 5),
                                 botm.Pos(15, 6),
                                 botm.Pos(15, 8),
                                 botm.Pos(10, 9)))
    assert path[4] == frozenset((botm.Pos(13, 3),
                                 botm.Pos(11, 5), botm.Pos(15, 5),
                                 botm.Pos(15, 9),
                                 botm.Pos(9, 9)))
    assert path[5] == frozenset((botm.Pos(13, 2),
                                 botm.Pos(12, 3),
                                 botm.Pos(11, 4),
                                 botm.Pos(10, 5), botm.Pos(16, 5),
                                 botm.Pos(11, 6),
                                 botm.Pos(16, 9),
                                 botm.Pos(8, 9)))

    test(MAZE2)

    # maze3
    path = MAZE3.paths(botm.Pos(29, 3))

    assert path[0] == frozenset((botm.Pos(28, 3), botm.Pos(29, 4)))
    assert path[1] == frozenset((botm.Pos(27, 3), botm.Pos(29, 5)))
    assert path[2] == frozenset((botm.Pos(27, 2),
                                 botm.Pos(28, 5),
                                 botm.Pos(29, 6)))
    assert path[3] == frozenset((botm.Pos(27, 1),
                                 botm.Pos(27, 5),
                                 botm.Pos(29, 7)))
    assert path[4] == frozenset((botm.Pos(28, 1),
                                 botm.Pos(26, 5),
                                 botm.Pos(27, 6)))
    assert path[5] == frozenset((botm.Pos(29, 1),
                                 botm.Pos(25, 5),
                                 botm.Pos(27, 7)))
    assert path[6] == frozenset((botm.Pos(30, 1),
                                 botm.Pos(25, 4),
                                 botm.Pos(25, 6),
                                 botm.Pos(26, 7)))
    assert path[7] == frozenset((botm.Pos(0, 1),
                                 botm.Pos(25, 3),
                                 botm.Pos(25, 7)))
    assert path[8] == frozenset((botm.Pos(1, 1),
                                 botm.Pos(25, 2),
                                 botm.Pos(24, 3),
                                 botm.Pos(25, 8)))

    test(MAZE3)


def test__possible_moves():
    # maze0
    assert MAZE0.possible_moves(botm.Pos(1, 1)) == (botm.Pos(1, 2), )

    list0 = list(MAZE0.possible_moves(botm.Pos(1, 2)))
    list0.sort()
    list1 = [botm.Pos(*t) for t in [(1, 1), (0, 2)]]

    assert list0 == list1

    list0 = list(MAZE0.possible_moves(botm.Pos(0, 2)))
    list0.sort()
    list1 = [botm.Pos(*t) for t in [(1, 2), (4, 2)]]

    assert list0 == list1

    # maze1
    list0 = list(MAZE1.possible_moves(botm.Pos(21, 3)))
    list0.sort()
    list1 = [botm.Pos(*t) for t in [(20, 3), (22, 3), (21, 4)]]

    assert list0 == list1

    # maze2
    list0 = list(MAZE2.possible_moves(botm.Pos(23, 3)))
    list0.sort()
    list1 = [botm.Pos(*t) for t in [(23, 2), (22, 3), (24, 3)]]

    assert list0 == list1


def test__repr():
    assert repr(MAZE0) == 'Maze("""{}""")'.format(MAZE0_STR)
    assert repr(MAZE1) == 'Maze("""{}""")'.format(MAZE1_STR)
    assert repr(MAZE2) == 'Maze("""{}""")'.format(MAZE2_STR)
    assert repr(MAZE3) == 'Maze("""{}""")'.format(MAZE3_STR)


def test__search_down_wall():
    # maze0
    assert MAZE0.search_up_wall(botm.Pos(1, 1)) == botm.Pos(1, 0)
    assert MAZE0.search_up_wall(botm.Pos(1, 2)) == botm.Pos(1, 0)

    assert MAZE0.search_down_wall(botm.Pos(1, 1)) == botm.Pos(1, 3)
    assert MAZE0.search_down_wall(botm.Pos(1, 2)) == botm.Pos(1, 3)

    assert MAZE0.search_left_wall(botm.Pos(1, 1)) == botm.Pos(0, 1)
    assert MAZE0.search_left_wall(botm.Pos(1, 2)) == botm.Pos(2, 2)
    assert MAZE0.search_left_wall(botm.Pos(0, 2)) == botm.Pos(2, 2)
    assert MAZE0.search_left_wall(botm.Pos(3, 2)) == botm.Pos(2, 2)

    assert MAZE0.search_right_wall(botm.Pos(1, 1)) == botm.Pos(2, 1)
    assert MAZE0.search_right_wall(botm.Pos(1, 2)) == botm.Pos(2, 2)
    assert MAZE0.search_right_wall(botm.Pos(0, 2)) == botm.Pos(2, 2)
    assert MAZE0.search_right_wall(botm.Pos(3, 2)) == botm.Pos(2, 2)


def test__size():
    assert MAZE0.width == 5
    assert MAZE0.height == 4

    assert MAZE1.width == 33
    assert MAZE1.height == 12

    assert MAZE2.width == 33
    assert MAZE2.height == 12

    assert MAZE3.width == 31
    assert MAZE3.height == 13


def test__symmetry():
    # maze0
    for pos_y in range(4):
        for pos_x in range(5):
            pos = botm.Pos(pos_x, pos_y)

            assert MAZE0.get_cell(pos) == MAZE0.get_cell(botm.Pos(5 - 1 - pos_x, pos_y))  # noqa
            assert MAZE0.get_cell(pos) == MAZE0.get_cell(MAZE0.symmetrical(pos))  # noqa

    # maze1
    for pos_y in range(12):
        for pos_x in range(33):
            pos = botm.Pos(pos_x, pos_y)

            assert MAZE1.get_cell(pos) == MAZE1.get_cell(botm.Pos(33 - 1 - pos_x, pos_y))  # noqa
            assert MAZE1.get_cell(pos) == MAZE1.get_cell(MAZE1.symmetrical(pos))  # noqa

    # maze2
    for pos_y in range(12):
        for pos_x in range(33):
            pos = botm.Pos(pos_x, pos_y)

            assert MAZE2.get_cell(pos) == MAZE2.get_cell(botm.Pos(33 - 1 - pos_x, pos_y))  # noqa
            assert MAZE2.get_cell(pos) == MAZE2.get_cell(MAZE2.symmetrical(pos))  # noqa

    # maze3
    for pos_y in range(13):
        for pos_x in range(31):
            cell0 = MAZE3.get_cell(botm.Pos(pos_x, pos_y))
            cell1 = MAZE3.get_cell(botm.Pos(31 - 1 - pos_x, pos_y))

            if (cell0 == botm.Maze.WALL) or (cell1 == botm.Maze.WALL):
                assert cell0 == cell1

            if cell0 != botm.Maze.WALL:
                assert cell1 != botm.Maze.WALL
            if cell1 != botm.Maze.WALL:
                assert cell0 != botm.Maze.WALL


def test__str():
    assert str(MAZE0) == MAZE0_STR.replace('5 4\n', '')
    assert str(MAZE1) == MAZE1_STR.replace('33 12\n', '')
    assert str(MAZE2) == MAZE2_STR.replace('33 12\n', '')
    assert str(MAZE3) == MAZE3_STR.replace('31 13\n', '')


def test__symmetrical():
    assert MAZE1.symmetrical(botm.Pos(0, 9)) == botm.Pos(32, 9)
    assert MAZE1.symmetrical(botm.Pos(1, 9)) == botm.Pos(31, 9)
    assert MAZE1.symmetrical(botm.Pos(2, 9)) == botm.Pos(30, 9)

    assert MAZE1.symmetrical(botm.Pos(32, 9)) == botm.Pos(0, 9)
    assert MAZE1.symmetrical(botm.Pos(31, 9)) == botm.Pos(1, 9)
    assert MAZE1.symmetrical(botm.Pos(30, 9)) == botm.Pos(2, 9)

    assert MAZE1.symmetrical(botm.Pos(15, 9)) == botm.Pos(17, 9)
    assert MAZE1.symmetrical(botm.Pos(16, 9)) == botm.Pos(16, 9)
    assert MAZE1.symmetrical(botm.Pos(17, 9)) == botm.Pos(15, 9)


def test__to_str():
    assert MAZE0.to_str() == MAZE0_STR.replace('5 4\n', '')
    assert MAZE1.to_str() == MAZE1_STR.replace('33 12\n', '')
    assert MAZE2.to_str() == MAZE2_STR.replace('33 12\n', '').replace('2', ' ').replace('3', ' ').replace('4', ' ').replace('5', ' ')  # noqa
    assert MAZE3.to_str() == MAZE3_STR.replace('31 13\n', '').replace('2', ' ').replace('3', ' ').replace('4', ' ')  # noqa

    assert MAZE0.to_str({botm.Pos(1, 2): 'x', botm.Pos(3, 1): 'M',
                         botm.Pos(4, 2): '.'}) == """#####
# #M#
 x# .
#####"""


def test__visible_poss():
    # maze0
    assert MAZE0.visible_poss(botm.Pos(1, 1)) == frozenset((botm.Pos(1, 2), ))
    assert MAZE0.visible_poss(botm.Pos(1, 2)) == frozenset(
        (botm.Pos(1, 1),
         botm.Pos(0, 2), botm.Pos(3, 2), botm.Pos(4, 2)))
    assert MAZE0.visible_poss(botm.Pos(0, 2)) == frozenset(
        (botm.Pos(1, 2), botm.Pos(3, 2), botm.Pos(4, 2)))
    assert MAZE0.visible_poss(botm.Pos(4, 2)) == frozenset(
        (botm.Pos(0, 2), botm.Pos(1, 2), botm.Pos(3, 2)))
    assert MAZE0.visible_poss(botm.Pos(3, 2)) == frozenset(
        (botm.Pos(3, 1),
         botm.Pos(0, 2), botm.Pos(1, 2), botm.Pos(4, 2)))
    assert MAZE0.visible_poss(botm.Pos(3, 1)) == frozenset((botm.Pos(3, 2), ))

    # maze1
    assert MAZE1.visible_poss(botm.Pos(18, 7)) == frozenset(
        (botm.Pos(17, 7), botm.Pos(19, 7)))
    assert MAZE1.visible_poss(botm.Pos(17, 7)) == frozenset(
        (botm.Pos(17, 5),
         botm.Pos(17, 6),
         botm.Pos(18, 7), botm.Pos(19, 7),
         botm.Pos(17, 8),
         botm.Pos(17, 9)))
