# -*- coding: utf-8 -*-

"""
test__botm__pos.py.

Test of class Pos.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 15, 2020
"""

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm


def test__pos():
    pos_o = botm.Pos(0, 0)
    pos_a = botm.Pos(3, 5)
    pos_b = botm.Pos(18, 2)

    assert pos_o.x == 0
    assert pos_o.y == 0

    assert pos_a.x == 3
    assert pos_a.y == 5

    assert pos_b.x == 18
    assert pos_b.y == 2

    assert pos_o.distance(pos_a) == 8
    assert pos_o.distance(pos_b) == 20
    assert pos_a.distance(pos_b) == 18

    assert repr(pos_o) == 'Pos(0, 0)'
    assert repr(pos_a) == 'Pos(3, 5)'
    assert repr(pos_b) == 'Pos(18, 2)'

    assert str(pos_o) == '0 0'
    assert str(pos_a) == '3 5'
    assert str(pos_b) == '18 2'

    assert hash(pos_b) == hash(botm.Pos(18, 2))

    for pos0 in (pos_o, pos_a, pos_b):
        assert repr(pos0) == 'Pos({})'.format(str(pos0).replace(' ', ', '))
        assert pos0 != None  # noqa  # pylint: disable=singleton-comparison
        assert None != pos0  # noqa  # pylint: disable=misplaced-comparison-constant

        for pos1 in (pos_o, pos_a, pos_b):
            assert pos0.distance(pos1) == pos1.distance(pos0)

            if id(pos0) == id(pos1):
                assert pos0 == pos1
                assert hash(pos0) == hash(pos1)
                assert pos0.distance(pos1) == 0
            else:
                assert pos0 != pos1
                assert hash(pos0) != hash(pos1)  # in fact this is not sure
                assert pos0.distance(pos1) > 0


def test__symmetrical():
    assert botm.Pos(0, 42).symmetrical(36) == botm.Pos(35, 42)
    assert botm.Pos(1, 42).symmetrical(36) == botm.Pos(34, 42)
    assert botm.Pos(2, 42).symmetrical(36) == botm.Pos(33, 42)

    assert botm.Pos(0, 42).symmetrical(35) == botm.Pos(34, 42)
    assert botm.Pos(1, 42).symmetrical(35) == botm.Pos(33, 42)
    assert botm.Pos(2, 42).symmetrical(35) == botm.Pos(32, 42)

    #
    assert botm.Pos(35, 42).symmetrical(36) == botm.Pos(0, 42)
    assert botm.Pos(34, 42).symmetrical(36) == botm.Pos(1, 42)
    assert botm.Pos(33, 42).symmetrical(36) == botm.Pos(2, 42)

    assert botm.Pos(34, 42).symmetrical(35) == botm.Pos(0, 42)
    assert botm.Pos(33, 42).symmetrical(35) == botm.Pos(1, 42)
    assert botm.Pos(32, 42).symmetrical(35) == botm.Pos(2, 42)

    #
    assert botm.Pos(17, 42).symmetrical(36) == botm.Pos(18, 42)
    assert botm.Pos(18, 42).symmetrical(36) == botm.Pos(17, 42)

    assert botm.Pos(16, 42).symmetrical(35) == botm.Pos(18, 42)
    assert botm.Pos(17, 42).symmetrical(35) == botm.Pos(17, 42)
    assert botm.Pos(18, 42).symmetrical(35) == botm.Pos(16, 42)
