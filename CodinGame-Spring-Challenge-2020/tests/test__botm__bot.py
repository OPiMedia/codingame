# -*- coding: utf-8 -*-

"""
test__botm__bot.py.

Test of class Bot.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 18, 2020
"""

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm


def test__():
    reader = botm.Reader()

    reader.add_lines("""33 12
#################################
#       #   #   #   #   #       #
# # ##### ### # # # ### ##### # #
# #           #   #           # #
# ### ##### # ##### # ##### ### #
#     #       #   #       #     #
##### # ### # # # # # ### # #####
#           #   #   #           #
# # ### ##### # # # ##### ### # #
# #   #       #   #       #   # #
##### # ##### ##### ##### # #####
#################################
42 666
1
0 1 3 2 ROCK 0 0
1
4 5 10""".split('\n'))

    botm.Bot(0, reader)
