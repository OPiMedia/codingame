# -*- coding: utf-8 -*-

"""
test__botm__pacmine.py.

Test specific methods of class PacMine.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- HTTP://www.opimedia.be/
:version: May 17, 2020
"""

import pytest  # type: ignore  # pylint: disable=unused-import,unused-argument

import botm


def test__test_command():  # pylint: disable=too-many-statements
    # Add my pac
    pac = botm.PacMine(0, 36, botm.Pos(13, 21), botm.Pac.SCISSORS, 55, 66)

    assert pac.destination is None
    assert pac.to_speed is False
    assert pac.to_switch is None

    command = pac.command()

    assert command == ''
    assert pac.destination is None
    assert pac.to_speed is False
    assert pac.to_switch is None

    # destination
    pac.set_destination(botm.Pos(5, 0))

    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is False
    assert pac.to_switch is None

    command = pac.command()

    assert command == 'MOVE 36 5 0 5 0'
    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is False
    assert pac.to_switch is None

    # destination
    pac.set_destination(botm.Pos(13, 21))

    assert pac.destination is None
    assert pac.to_speed is False
    assert pac.to_switch is None

    command = pac.command()

    assert pac.command() == ''
    assert pac.destination is None
    assert pac.to_speed is False
    assert pac.to_switch is None

    # speed and destination
    pac.set_speed()
    pac.set_destination(botm.Pos(5, 0))

    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is True
    assert pac.to_switch is None

    command = pac.command()

    assert command == 'SPEED 36'
    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is False
    assert pac.to_switch is None

    # speed, switch and destination
    pac.set_speed()
    pac.set_switch(botm.Pac.ROCK)
    pac.set_destination(botm.Pos(5, 0))

    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is True
    assert pac.to_switch == botm.Pac.ROCK

    command = pac.command()

    assert command == 'SWITCH 36 ROCK'
    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is True
    assert pac.to_switch is None

    command = pac.command()

    assert command == 'SPEED 36'
    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is False
    assert pac.to_switch is None

    command = pac.command()

    assert command == 'MOVE 36 5 0 5 0'
    assert pac.destination == botm.Pos(5, 0)
    assert pac.to_speed is False
    assert pac.to_switch is None


def test__set_destination():
    # Add my pac
    pac = botm.PacMine(0, 36, botm.Pos(13, 21), botm.Pac.SCISSORS, 55, 66)

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch None'  # noqa

    pac.set_destination(botm.Pos(5, 0))

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (5 0) blocked 0 to_speed False to_switch None'  # noqa

    pac.set_destination(botm.Pos(13, 21))

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch None'  # noqa


def test__set_speed():
    # Add my pac
    pac = botm.PacMine(0, 36, botm.Pos(13, 21), botm.Pac.SCISSORS, 55, 66)

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch None'  # noqa

    pac.set_speed()

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed True to_switch None'  # noqa


def test__set_switch():
    # Add my pac
    pac = botm.PacMine(0, 36, botm.Pos(13, 21), botm.Pac.SCISSORS, 55, 66)

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch None'  # noqa

    pac.set_switch(botm.Pac.ROCK)

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch 0'  # noqa

    pac.set_switch(botm.Pac.SCISSORS)

    assert str(pac) == 'PacMine 36 (13 21)->(13 21) SCISSORS speed 55 ability 66 destination (None) blocked 0 to_speed False to_switch None'  # noqa
